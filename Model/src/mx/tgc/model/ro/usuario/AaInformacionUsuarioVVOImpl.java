package mx.tgc.model.ro.usuario;

import mx.tgc.utilityfwk.model.extension.view.GenericViewImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Dec 19 15:50:04 MST 2014
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AaInformacionUsuarioVVOImpl extends GenericViewImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public AaInformacionUsuarioVVOImpl() {
    }


    /**
     * Returns the variable value for p_usuario.
     * @return variable value for p_usuario
     */
    public String getp_usuario() {
        return (String)ensureVariableManager().getVariableValue("p_usuario");
    }

    /**
     * Sets <code>value</code> for variable p_usuario.
     * @param value value to bind as p_usuario
     */
    public void setp_usuario(String value) {
        ensureVariableManager().setVariableValue("p_usuario", value);
    }
}
