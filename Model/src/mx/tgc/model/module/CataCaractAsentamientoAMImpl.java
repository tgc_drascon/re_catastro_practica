package mx.tgc.model.module;

import java.util.HashMap;
import java.util.Map;

import mx.tgc.model.module.common.CataCaractAsentamientoAM;
import mx.tgc.model.module.generic.CatastroAMImpl;
import mx.tgc.model.ro.ubicacion.AaUsuariosMunicipiosVVOImpl;
import mx.tgc.model.ro.ubicacion.PcAsentamientosVVOImpl;
import mx.tgc.model.ro.ubicacion.PcLocalidadesVVOImpl;
import mx.tgc.model.ro.ubicacion.PcLocalidadesVVORowImpl;
import mx.tgc.model.ro.ubicacion.PcMunicipiosRecaudacionesCustomVVOImpl;
import mx.tgc.model.ro.ubicacion.PcMunicipiosVVOImpl;
import mx.tgc.model.ro.ubicacion.PcMunicipiosVVORowImpl;
import mx.tgc.model.view.catastro.PrCaracteristicasAsentamientosVOImpl;
import mx.tgc.model.view.movimiento.PrPredialCaracteristicasVOImpl;
import mx.tgc.model.view.movimiento.PrTiposAvaluosVOImpl;
import mx.tgc.model.view.objeto.PcAsentamientosVOImpl;
import mx.tgc.model.view.objeto.PcAsentamientosVORowImpl;
import mx.tgc.model.view.ubicacion.PcVialidadesVOImpl;
import mx.tgc.model.view.ubicacion.PcVialidadesVORowImpl;
import mx.tgc.utilityfwk.model.extension.view.GenericViewImpl;
import oracle.jbo.server.ViewLinkImpl;
import oracle.jbo.server.ViewObjectImpl;


// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Aug 19 11:53:43 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class CataCaractAsentamientoAMImpl extends CatastroAMImpl implements CataCaractAsentamientoAM {
    /**
     * This is the default constructor (do not remove).
     */
    public CataCaractAsentamientoAMImpl() {
    }

    /**
     * Este m�todo se encarga de filtrar la vista custom de PcMunicipiosRecaudaciones
     * para poder filtrar posteriormente las localidades, este m�todo
     * se debe mandar llamar en el prepareModel() en el managed bean de la
     * pantalla del cat�logo de vialidades.
     *
     * @param idRecaudacion String con el id de la recaudaci�n base
     *        del usuario firmado.
     *
     * @author Eduardo Vidal
     * @date 19-08-2011
     */

    public void filtrarMunicipiosXRecaudacion(String idRecaudacion) {
        PcMunicipiosRecaudacionesCustomVVOImpl view =
            this.getPcMunicipiosRecaudacionesCustomVVO1();
        view.setWhereClause("pc_reca_identificador = " + idRecaudacion);
        view.executeQuery();
    }
    
    /**
     * Este m�todo se encarga de filtrar la vista AaUsuariosMunicipios, resiviendo como 
     * par�metro una cadena que representa el o los municipios asociados al usuario,
     * esta cadena puede estar en el siguiente formato:
     * 
     *  # 1
     *  # 1,2,3
     * 
     * El m�todo soporta cadenas vacias "" o valores nulos, en tal caso la vista se consulta
     * sin filtros.
     *
     * @param muniId String con los identificadores de los municipios asociados al usuario
     *
     * @author Ricardo Rey
     * @date 30-06-2015
     */
    public void filtrarMunicipiosXUsuario(String muniId) {
        AaUsuariosMunicipiosVVOImpl view = this.getAaUsuariosMunicipiosVVO1();
        if (muniId != null && !"".equals(muniId)){
            view.setWhereClause("identificador IN (" + muniId +")");
        }else{
            view.setWhereClause(null);    
        }
        view.executeQuery();
    }

    /**
     * Valida que la caracter�stica del asentamiento no est� repetida antes de insertarla
     *
     * @param asenId
     * @return true si la caracter�stica puede insertarse sin problemas
     * @author enunez
     * @date 29/09/2011
     */
    public void filtrarCaractAsentamientos(String asenId) {
        //Aqu� se hace el filtrado de la tabla de caracter�sticas que se muestra al usuario para seleccionar
        PrPredialCaracteristicasVOImpl view = getPrPredialCaracteristicasVO2();
        //Comparo todos los Series y los Ids de la tabla de predial caracter�sticas que se muestra al usuario para seleccionar contra la tabla de ya seleccionados
        view.setWhereClause("(SERIE, IDENTIFICADOR) NOT IN (SELECT PR_PRCA_SERIE, PR_PRCA_IDENTIFICADOR FROM PR_CARACT_ASENTAMIENTOS WHERE PC_ASEN_IDENTIFICADOR = " +
                            asenId + ")");
        view.executeQuery();
        view.reset();
    }

    /**
     * Valida que la caracter�stica del asentamiento no est� repetida antes de insertarla
     *
     * @param ccRecaId
     * @return true si la caracter�stica puede insertarse sin problemas
     * @author enunez
     * @date 29/09/2011
     * 
     * Se cambia el filtrado de un where clause a un view criteria para evitar 
     * ambig�edad en el nombre de columnas.
     * @modify 18/12/2013 Se cambia el filtrado de un where clause a un 
     * view criteria para evitar ambig�edad en el nombre de columnas.
     * @author Antuan Ya�ez
     */
    public void filtrarTiposAvaluos(String muniId) {
        //filtra todos los tipos de aval�os que traigan este ccRecaId
        if (muniId != null) {
            PrTiposAvaluosVOImpl view = getPrTiposAvaluosVO1();
            view.setApplyViewCriteriaName("findByMunicipio");
            view.setNamedWhereClauseParam("muniId",muniId);
            view.reset();
        }
    }

    /*
     * Accessors generados autom�ticamente
     */

    /**
     * Container's getter for PcMunicipiosRecaudacionesCustomVVO1.
     * @return PcMunicipiosRecaudacionesCustomVVO1
     */
    public PcMunicipiosRecaudacionesCustomVVOImpl getPcMunicipiosRecaudacionesCustomVVO1() {
        return (PcMunicipiosRecaudacionesCustomVVOImpl)findViewObject("PcMunicipiosRecaudacionesCustomVVO1");
    }

    /**
     * Container's getter for PcLocalidadesVVO1.
     * @return PcLocalidadesVVO1
     */
    public PcLocalidadesVVOImpl getPcLocalidadesVVO1() {
        return (PcLocalidadesVVOImpl)findViewObject("PcLocalidadesVVO1");
    }

    /**
     * Container's getter for PcRecaMuniPcLocaFkLink1.
     * @return PcRecaMuniPcLocaFkLink1
     */
    public ViewLinkImpl getPcRecaMuniPcLocaFkLink1() {
        return (ViewLinkImpl)findViewLink("PcRecaMuniPcLocaFkLink1");
    }

    /**
     * Container's getter for PcVialidadesVO1.
     * @return PcVialidadesVO1
     */
    public PcVialidadesVOImpl getPcVialidadesVO1() {
        return (PcVialidadesVOImpl)findViewObject("PcVialidadesVO1");
    }

    /**
     * Container's getter for PcLocaPcVialiFkLink1.
     * @return PcLocaPcVialiFkLink1
     */
    public ViewLinkImpl getPcLocaPcVialiFkLink1() {
        return (ViewLinkImpl)findViewLink("PcLocaPcVialiFkLink1");
    }

    /**
     * Container's getter for PcAsentamientosVVO1.
     * @return PcAsentamientosVVO1
     */
    public PcAsentamientosVVOImpl getPcAsentamientosVVO1() {
        return (PcAsentamientosVVOImpl)findViewObject("PcAsentamientosVVO1");
    }

    /**
     * Container's getter for PrCaractAsentamientosVO1.
     * @return PrCaractAsentamientosVO1
     */
    public ViewObjectImpl getPrCaractAsentamientosVO1() {
        return (ViewObjectImpl)findViewObject("PrCaractAsentamientosVO1");
    }

    /**
     * Container's getter for PcAsenPrCaractAsenFKLink1.
     * @return PcAsenPrCaractAsenFKLink1
     */
    public ViewLinkImpl getPcAsenPrCaractAsenFKLink1() {
        return (ViewLinkImpl)findViewLink("PcAsenPrCaractAsenFKLink1");
    }


    /**
     * Container's getter for PcAsentamientosVO2.
     * @return PcAsentamientosVO2
     */
    public PcAsentamientosVOImpl getPcAsentamientosVO2() {
        return (PcAsentamientosVOImpl)findViewObject("PcAsentamientosVO2");
    }

    /**
     * Container's getter for PcLocaPcAsenFkLink1.
     * @return PcLocaPcAsenFkLink1
     */
    public ViewLinkImpl getPcLocaPcAsenFkLink1() {
        return (ViewLinkImpl)findViewLink("PcLocaPcAsenFkLink1");
    }


    /**
     * Container's getter for PrCaractAsentamientosVO2.
     * @return PrCaractAsentamientosVO2
     */
    public GenericViewImpl getPrCaractAsentamientosVO2() {
        return (GenericViewImpl)findViewObject("PrCaractAsentamientosVO2");
    }

    /**
     * Container's getter for PcAsPrCaasFkLink1.
     * @return PcAsPrCaasFkLink1
     */
    public ViewLinkImpl getPcAsPrCaasFkLink1() {
        return (ViewLinkImpl)findViewLink("PcAsPrCaasFkLink1");
    }

    /**
     * Container's getter for PrPredialCaracteristicasVO1.
     * @return PrPredialCaracteristicasVO1
     */
    public PrPredialCaracteristicasVOImpl getPrPredialCaracteristicasVO1() {
        return (PrPredialCaracteristicasVOImpl)findViewObject("PrPredialCaracteristicasVO1");
    }

    /**
     * Container's getter for PrPrcaPrCaasFkLink1.
     * @return PrPrcaPrCaasFkLink1
     */
    public ViewLinkImpl getPrPrcaPrCaasFkLink1() {
        return (ViewLinkImpl)findViewLink("PrPrcaPrCaasFkLink1");
    }

    /**
     * Container's getter for PrTiposAvaluosVO1.
     * @return PrTiposAvaluosVO1
     */
    public PrTiposAvaluosVOImpl getPrTiposAvaluosVO1() {
        return (PrTiposAvaluosVOImpl)findViewObject("PrTiposAvaluosVO1");
    }

    /**
     * Container's getter for PrPredialCaracteristicasVO2.
     * @return PrPredialCaracteristicasVO2
     */
    public PrPredialCaracteristicasVOImpl getPrPredialCaracteristicasVO2() {
        return (PrPredialCaracteristicasVOImpl)findViewObject("PrPredialCaracteristicasVO2");
    }

    /**
     * Container's getter for PrPrcaPrTiavFkLink1.
     * @return PrPrcaPrTiavFkLink1
     */
    public ViewLinkImpl getPrPrcaPrTiavFkLink1() {
        return (ViewLinkImpl)findViewLink("PrPrcaPrTiavFkLink1");
    }

    /**
     * Container's getter for PrCaracteristicasAsentamientosVO1.
     * @return PrCaracteristicasAsentamientosVO1
     */
    public PrCaracteristicasAsentamientosVOImpl getPrCaracteristicasAsentamientosVO1() {
        return (PrCaracteristicasAsentamientosVOImpl)findViewObject("PrCaracteristicasAsentamientosVO1");
    }
    
    public Map validaClaveAsentamiento(String claveAsentamiento){
        Map resultado=new HashMap();
        String localidadIdentificador=getlocalidadIdentificadorXclaveAsentamiento(claveAsentamiento);
        if(!localidadIdentificador.equals("")){
            String pcMuniIdentificador=getPcMuniIdentificadorXPcLocaIdentificador(localidadIdentificador);
            if(!pcMuniIdentificador.equals("")){
                String nombreLocalidad=getNombreLocalidadXlocalidadIdentificador(localidadIdentificador);
                if(!nombreLocalidad.equals("")){
                    String nombreMunicipio=getNombreMunicipioXpcMuniIdentificador(pcMuniIdentificador);
                    if(nombreMunicipio!=null&&!nombreMunicipio.equals("")){
                        resultado.put("resultado", "true");
                        resultado.put("municipio",nombreMunicipio);
                        resultado.put("localidad",nombreLocalidad);
                    }
                }
               
            }
           
        }
        return resultado;
    }
    
    public Map validaClaveVialidad(String claveVialidad){
        Map resultado=new HashMap();
        String localidadIdentificador=getlocalidadIdentificadorXclaveVialidad(claveVialidad);
        if(!localidadIdentificador.equals("")){
            String pcMuniIdentificador=getPcMuniIdentificadorXPcLocaIdentificador(localidadIdentificador);
            if(!pcMuniIdentificador.equals("")){
                String nombreLocalidad=getNombreLocalidadXlocalidadIdentificador(localidadIdentificador);
                if(!nombreLocalidad.equals("")){
                    String nombreMunicipio=getNombreMunicipioXpcMuniIdentificador(pcMuniIdentificador);
                    if(nombreMunicipio!=null&&!nombreMunicipio.equals("")){
                        resultado.put("resultado", "true");
                        resultado.put("municipio",nombreMunicipio);
                        resultado.put("localidad",nombreLocalidad);
                    }
                }
               
            }
           
        }
        return resultado;
    }
    
    private String getlocalidadIdentificadorXclaveVialidad(String claveVialidad){
        String res="";
        PcVialidadesVOImpl view = getPcVialidadesVO2();
        view.setApplyViewCriteriaName("filtrarVialidadXClaveVialidad");
        view.setNamedWhereClauseParam("claveVialidad",claveVialidad);
        view.executeQuery();
        
        if(view.getRowCount()>0){
                PcVialidadesVORowImpl row = (PcVialidadesVORowImpl)view.next();
                res =row.getPcLocaIdentificador().toString();
        }
        view.removeApplyViewCriteriaName("filtrarVialidadXClaveVialidad");
        
        return res;
    }
    
    
    private String getNombreLocalidadXlocalidadIdentificador(String localidadIdentificador){
        String res="";
        PcLocalidadesVVOImpl view = getPcLocalidadesVVO2();
        view.setApplyViewCriteriaName("filtrarLocalidadesXIdentificador");
        view.setNamedWhereClauseParam("identificador",localidadIdentificador);
        view.executeQuery();
        
        if(view.getRowCount()>0){
            PcLocalidadesVVORowImpl row = (PcLocalidadesVVORowImpl)view.next();
            res =row.getDescripcion().toString();
        }
        view.removeApplyViewCriteriaName("filtrarLocalidadesXIdentificador");
        return res;
    }
    
    private String getNombreMunicipioXpcMuniIdentificador(String pcMuniIdentificador){
        String res="";
        PcMunicipiosVVOImpl view = getPcMunicipiosVVO1();
        view.setWhereClause("IDENTIFICADOR = " + pcMuniIdentificador);
        view.setApplyViewCriteriaName("filtrarMunicipiosActivos");
        view.executeQuery();
        
        if(view.getRowCount()>0){
            PcMunicipiosVVORowImpl row = (PcMunicipiosVVORowImpl)view.next();
            res=row.getNombreCorto();
        } 
        view.removeApplyViewCriteriaName("filtrarMunicipiosActivos");
        view.setWhereClause(null);
        return res;
    }
    
    private String getPcMuniIdentificadorXPcLocaIdentificador(String localidadIdentificador ){
        String res="";
        PcLocalidadesVVOImpl view = getPcLocalidadesVVO2();
        view.setApplyViewCriteriaName("filtrarLocalidadesXIdentificador");
        view.setNamedWhereClauseParam("identificador",localidadIdentificador);
        view.executeQuery();
        
        if(view.getRowCount()>0){
            PcLocalidadesVVORowImpl row = (PcLocalidadesVVORowImpl)view.next();
            res =row.getPcMuniIdentificador().toString();
        }
        view.removeApplyViewCriteriaName("filtrarLocalidadesXIdentificador");
        return res;
    }
    
    private String getlocalidadIdentificadorXclaveAsentamiento(String claveAsentamiento){
        String res="";
        PcAsentamientosVOImpl view = getPcAsentamientosVO1();
        view.setApplyViewCriteriaName("filtrarClaveAsentamiento");
        view.setNamedWhereClauseParam("claveAsentamiento",claveAsentamiento);
        view.executeQuery();
        
        if(view.getRowCount()>0){
                PcAsentamientosVORowImpl row = (PcAsentamientosVORowImpl)view.next();
                res =row.getPcLocaIdentificador().toString();
        }
        view.removeApplyViewCriteriaName("filtrarClaveAsentamiento");
        return res;
    }

    /**
     * Container's getter for PcLocalidadesVVO2.
     * @return PcLocalidadesVVO2
     */
    public PcLocalidadesVVOImpl getPcLocalidadesVVO2() {
        return (PcLocalidadesVVOImpl)findViewObject("PcLocalidadesVVO2");
    }

    /**
     * Container's getter for PcMunicipiosVVO1.
     * @return PcMunicipiosVVO1
     */
    public PcMunicipiosVVOImpl getPcMunicipiosVVO1() {
        return (PcMunicipiosVVOImpl)findViewObject("PcMunicipiosVVO1");
    }

    /**
     * Container's getter for PcAsentamientosVO1.
     * @return PcAsentamientosVO1
     */
    public PcAsentamientosVOImpl getPcAsentamientosVO1() {
        return (PcAsentamientosVOImpl)findViewObject("PcAsentamientosVO1");
    }

    /**
     * Container's getter for PcVialidadesVO2.
     * @return PcVialidadesVO2
     */
    public PcVialidadesVOImpl getPcVialidadesVO2() {
        return (PcVialidadesVOImpl)findViewObject("PcVialidadesVO2");
    }


    /**
     * Container's getter for AaUsuariosMunicipiosVVO1.
     * @return AaUsuariosMunicipiosVVO1
     */
    public AaUsuariosMunicipiosVVOImpl getAaUsuariosMunicipiosVVO1() {
        return (AaUsuariosMunicipiosVVOImpl)findViewObject("AaUsuariosMunicipiosVVO1");
    }

    /**
     * Container's getter for PcLocalidadesVVO3.
     * @return PcLocalidadesVVO3
     */
    public PcLocalidadesVVOImpl getPcLocalidadesVVO3() {
        return (PcLocalidadesVVOImpl)findViewObject("PcLocalidadesVVO3");
    }

    /**
     * Container's getter for AaUsuMuniPcLocaFkLink1.
     * @return AaUsuMuniPcLocaFkLink1
     */
    public ViewLinkImpl getAaUsuMuniPcLocaFkLink1() {
        return (ViewLinkImpl)findViewLink("AaUsuMuniPcLocaFkLink1");
    }


    /**
     * Container's getter for PcAsentamientosVO3.
     * @return PcAsentamientosVO3
     */
    public PcAsentamientosVOImpl getPcAsentamientosVO3() {
        return (PcAsentamientosVOImpl)findViewObject("PcAsentamientosVO3");
    }
    
    /**
     * Container's getter for PrCaractAsentamientosVO3.
     * @return PrCaractAsentamientosVO3
     */
    public GenericViewImpl getPrCaractAsentamientosVO3() {
        return (GenericViewImpl)findViewObject("PrCaractAsentamientosVO3");
    }

    /**
     * Container's getter for PcLocaPcAsenFkLink2.
     * @return PcLocaPcAsenFkLink2
     */
    public ViewLinkImpl getPcLocaPcAsenFkLink2() {
        return (ViewLinkImpl)findViewLink("PcLocaPcAsenFkLink2");
    }

    /**
     * Container's getter for PcVialidadesVO3.
     * @return PcVialidadesVO3
     */
    public PcVialidadesVOImpl getPcVialidadesVO3() {
        return (PcVialidadesVOImpl)findViewObject("PcVialidadesVO3");
    }

    /**
     * Container's getter for PcLocaPcVialiFkLink2.
     * @return PcLocaPcVialiFkLink2
     */
    public ViewLinkImpl getPcLocaPcVialiFkLink2() {
        return (ViewLinkImpl)findViewLink("PcLocaPcVialiFkLink2");
    }

    /**
     * Container's getter for PcAsPrCaasFkLink2.
     * @return PcAsPrCaasFkLink2
     */
    public ViewLinkImpl getPcAsPrCaasFkLink2() {
        return (ViewLinkImpl)findViewLink("PcAsPrCaasFkLink2");
    }

    /**
     * Container's getter for PrPredialCaracteristicasVO3.
     * @return PrPredialCaracteristicasVO3
     */
    public PrPredialCaracteristicasVOImpl getPrPredialCaracteristicasVO3() {
        return (PrPredialCaracteristicasVOImpl)findViewObject("PrPredialCaracteristicasVO3");
    }

    /**
     * Container's getter for PrPrcaPrCaasFkLink2.
     * @return PrPrcaPrCaasFkLink2
     */
    public ViewLinkImpl getPrPrcaPrCaasFkLink2() {
        return (ViewLinkImpl)findViewLink("PrPrcaPrCaasFkLink2");
    }
}
