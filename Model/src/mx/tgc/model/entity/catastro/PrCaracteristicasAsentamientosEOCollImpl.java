package mx.tgc.model.entity.catastro;

import mx.tgc.utilityfwk.model.extension.entity.GenericEntityCollImpl;

import oracle.jbo.server.EntityCache;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Aug 31 12:23:20 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PrCaracteristicasAsentamientosEOCollImpl extends GenericEntityCollImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public PrCaracteristicasAsentamientosEOCollImpl() {
    }
}
