package mx.tgc.model;

import mx.tgc.model.module.applicationModule.CataAvaluoAMFixture;
import mx.tgc.model.module.applicationModule.CataAvaluoAMTest;
import mx.tgc.model.module.applicationModule.CataBloquesPrediosAMFixture;
import mx.tgc.model.module.applicationModule.CataBloquesPrediosAMTest;
import mx.tgc.model.module.applicationModule.CataCaractAsentamientoAMFixture;
import mx.tgc.model.module.applicationModule.CataCaractAsentamientoAMTest;
import mx.tgc.model.module.applicationModule.CataCaracteristicasValuacionAMFixture;
import mx.tgc.model.module.applicationModule.CataCaracteristicasValuacionAMTest;
import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;
import mx.tgc.model.module.applicationModule.CataSegCatastralesAMTest;
import mx.tgc.model.module.applicationModule.CataUsoSueloAMFixture;
import mx.tgc.model.module.applicationModule.CataUsoSueloAMTest;
import mx.tgc.model.module.applicationModule.CatalogoSencilloAMFixture;
import mx.tgc.model.module.applicationModule.CatalogoSencilloAMTest;
import mx.tgc.model.module.view.CcRecaudacionesVVO1VO.CcRecaudacionesVVO1VOTest;
import mx.tgc.model.module.view.PcAsentamientosVO2VO.PcAsentamientosVO2VOTest;
import mx.tgc.model.module.view.PcAsentamientosVVO1VO.PcAsentamientosVVO1VOTest;
import mx.tgc.model.module.view.PcLocalidadesVVO1VO.PcLocalidadesVVO1VOTest;
import mx.tgc.model.module.view.PcMunicipiosRecaudacionesCustomVVO1VO.PcMunicipiosRecaudacionesCustomVVO1VOTest;
import mx.tgc.model.module.view.PcVialidadesVO1VO.PcVialidadesVO1VOTest;
import mx.tgc.model.module.view.PrAccionesVVO1VO.PrAccionesVVO1VOTest;
import mx.tgc.model.module.view.PrCaractAsentamientosVO1VO.PrCaractAsentamientosVO1VOTest;
import mx.tgc.model.module.view.PrConfiguracionesAccionesVO1VO.PrConfiguracionesAccionesVO1VOTest;
import mx.tgc.model.module.view.PrConfiguracionesElementosVO1VO.PrConfiguracionesElementosVO1VOTest;
import mx.tgc.model.module.view.PrElementosVVO1VO.PrElementosVVO1VOTest;
import mx.tgc.model.module.view.PrGirosVO1VO.PrGirosVO1VOTest;
import mx.tgc.model.module.view.PrGirosVO2VO.PrGirosVO2VOTest;
import mx.tgc.model.module.view.PrGirosVO3VO.PrGirosVO3VOTest;
import mx.tgc.model.module.view.PrPredBloquesAgrupacionesVO1VO.PrPredBloquesAgrupacionesVO1VOTest;
import mx.tgc.model.module.view.PrPredBloquesAgrupacionesVVO1VO.PrPredBloquesAgrupacionesVVO1VOTest;
import mx.tgc.model.module.view.PrPredialCaracteristicasVO1VO.PrPredialCaracteristicasVO1VOTest;
import mx.tgc.model.module.view.PrPredialValoresUnitariosVO1VO.PrPredialValoresUnitariosVO1VOTest;
import mx.tgc.model.module.view.PrPrediosVVO1VO.PrPrediosVVO1VOTest;
import mx.tgc.model.module.view.PrSegmentosCatastralesUbicaVO1VO.PrSegmentosCatastralesUbicaVO1VOTest;
import mx.tgc.model.module.view.PrSegmentosCatastralesVO1VO.PrSegmentosCatastralesVO1VOTest;
import mx.tgc.model.module.view.PrSegmentosCatastralesVO2VO.PrSegmentosCatastralesVO2VOTest;
import mx.tgc.model.module.view.PrSegmentosCatastralesVO3VO.PrSegmentosCatastralesVO3VOTest;
import mx.tgc.model.module.view.PrTiposAvaluos1VO.PrTiposAvaluos1VOTest;
import mx.tgc.model.module.view.PrTiposAvaluosVO1VO.PrTiposAvaluosVO1VOTest;
import mx.tgc.model.module.view.PrTiposMovimientosPredios1VO.PrTiposMovimientosPredios1VOTest;
import mx.tgc.model.module.view.PrTiposMovimientosPrediosVVO1VO.PrTiposMovimientosPrediosVVO1VOTest;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { PrTiposMovimientosPrediosVVO1VOTest.class,
                       PrTiposMovimientosPredios1VOTest.class,
                       PrTiposAvaluosVO1VOTest.class,
                       PrTiposAvaluos1VOTest.class,
                       PrSegmentosCatastralesVO3VOTest.class,
                       PrSegmentosCatastralesVO2VOTest.class,
                       PrSegmentosCatastralesVO1VOTest.class,
                       PrSegmentosCatastralesUbicaVO1VOTest.class,
                       PrPrediosVVO1VOTest.class,
                       PrPredialValoresUnitariosVO1VOTest.class,
                       PrPredialCaracteristicasVO1VOTest.class,
                       PrPredBloquesAgrupacionesVVO1VOTest.class,
                       PrPredBloquesAgrupacionesVO1VOTest.class,
                       PrGirosVO3VOTest.class, PrGirosVO2VOTest.class,
                       PrGirosVO1VOTest.class, PrElementosVVO1VOTest.class,
                       PrConfiguracionesElementosVO1VOTest.class,
                       PrConfiguracionesAccionesVO1VOTest.class,
                       PrCaractAsentamientosVO1VOTest.class,
                       PrAccionesVVO1VOTest.class, PcVialidadesVO1VOTest.class,
                       PcMunicipiosRecaudacionesCustomVVO1VOTest.class,
                       PcLocalidadesVVO1VOTest.class,
                       PcAsentamientosVVO1VOTest.class,
                       PcAsentamientosVO2VOTest.class,
                       CcRecaudacionesVVO1VOTest.class,
                       CatalogoSencilloAMTest.class, CataUsoSueloAMTest.class,
                       CataSegCatastralesAMTest.class,
                       CataCaracteristicasValuacionAMTest.class,
                       CataCaractAsentamientoAMTest.class,
                       CataBloquesPrediosAMTest.class,
                       CataAvaluoAMTest.class })
public class AllTestsGlobal {
    
  @BeforeClass
  public static void setUp() {
  }

  @AfterClass
  public static void tearDown() throws Exception {
      CataAvaluoAMFixture.getInstance().release();
      CataBloquesPrediosAMFixture.getInstance().release();
      CataCaractAsentamientoAMFixture.getInstance().release();
      CataCaracteristicasValuacionAMFixture.getInstance().release();
      CatalogoSencilloAMFixture.getInstance().release();
      CataSegCatastralesAMFixture.getInstance().release();
      CataUsoSueloAMFixture.getInstance().release();
  }
    
}
