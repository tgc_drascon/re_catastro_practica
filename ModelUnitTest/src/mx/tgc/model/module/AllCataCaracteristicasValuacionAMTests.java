package mx.tgc.model.module;

import mx.tgc.model.module.applicationModule.CataCaracteristicasValuacionAMFixture;
import mx.tgc.model.module.applicationModule.CataCaracteristicasValuacionAMTest;
import mx.tgc.model.module.view.PrPredialCaracteristicasVO1VO.PrPredialCaracteristicasVO1VOTest;
import mx.tgc.model.module.view.PrTiposAvaluosVO1VO.PrTiposAvaluosVO1VOTest;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { PrPredialCaracteristicasVO1VOTest.class,
                       PrTiposAvaluosVO1VOTest.class,
                       CataCaracteristicasValuacionAMTest.class })
public class AllCataCaracteristicasValuacionAMTests {
    @BeforeClass
    public static void setUp() {
    }

    @AfterClass
    public static void tearDown() throws Exception {
        CataCaracteristicasValuacionAMFixture.getInstance().release();
    }
}
