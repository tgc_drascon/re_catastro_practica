package mx.tgc.model.module;

import mx.tgc.model.module.applicationModule.CataCaractAsentamientoAMFixture;
import mx.tgc.model.module.applicationModule.CataCaractAsentamientoAMTest;
import mx.tgc.model.module.view.PcAsentamientosVO2VO.PcAsentamientosVO2VOTest;
import mx.tgc.model.module.view.PcAsentamientosVVO1VO.PcAsentamientosVVO1VOTest;
import mx.tgc.model.module.view.PcLocalidadesVVO1VO.PcLocalidadesVVO1VOTest;
import mx.tgc.model.module.view.PcMunicipiosRecaudacionesCustomVVO1VO.PcMunicipiosRecaudacionesCustomVVO1VOTest;
import mx.tgc.model.module.view.PcVialidadesVO1VO.PcVialidadesVO1VOTest;
import mx.tgc.model.module.view.PrCaractAsentamientosVO1VO.PrCaractAsentamientosVO1VOTest;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { PcLocalidadesVVO1VOTest.class,
                       PcMunicipiosRecaudacionesCustomVVO1VOTest.class,
                       PcAsentamientosVO2VOTest.class,
                       PrCaractAsentamientosVO1VOTest.class,
                       PcAsentamientosVVO1VOTest.class,
                       PcVialidadesVO1VOTest.class,
                       CataCaractAsentamientoAMTest.class })
public class AllCataCaractAsentamientoAMTests {
    @BeforeClass
    public static void setUp() {
    }

    @AfterClass
    public static void tearDown() throws Exception {
        CataCaractAsentamientoAMFixture.getInstance().release();
    }
}
