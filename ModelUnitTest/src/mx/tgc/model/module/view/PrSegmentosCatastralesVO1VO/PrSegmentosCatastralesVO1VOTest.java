package mx.tgc.model.module.view.PrSegmentosCatastralesVO1VO;

import java.sql.SQLException;
import java.sql.Statement;

import mx.tgc.model.module.CataSegCatastralesAMImpl;
import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Test;

public class PrSegmentosCatastralesVO1VOTest {
    private static CataSegCatastralesAMFixture fixture1 =
        CataSegCatastralesAMFixture.getInstance();
    private static CataSegCatastralesAMImpl am =
        (CataSegCatastralesAMImpl)fixture1.getApplicationModule();

    public PrSegmentosCatastralesVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrSegmentosCatastralesVO1");
        assertNotNull(view);
    }

    /**
     * M�todo que sirve para probar los campos obligatorios
     * verifica que efectivamente si no se llenan los campos obligatorios
     * al validar el nuevo registro debe arrojar error.
     *
     * @author: Victor Alejandro Venegas Villalobos
     * @since 20-SEP-2011
     */
    @Test
    public void testCamposObligatorios() {
        ViewObject view = am.findViewObject("PrSegmentosCatastralesVO1");
        Row row = view.createRow();

        //TODO: Por cada atributo obligatorio en la vista implementar lo siguiente:
        //NOTA: Los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Serie", 2100);
        row.setAttribute("Identificador", 999999999999999L);
        row.setAttribute("ClaveSegmento", "CLAVE");
        row.setAttribute("Descripcion", "DESCRIPCION");
        row.setAttribute("TipoSegmento", "MU");
        row.setAttribute("Nivel", 1);
        row.setAttribute("Estatus", "AC");
        row.setAttribute("CcRecaIdentificador", 1);

        //-------------
        row.validate();
        am.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar la longitud de los campos
     * de una vista de datos, el m�todo prueba que las longitudes
     * sean probadas con la m�xima longitud permitida para los
     * campos de tipo String (VARCHAR2).
     *
     * @author: Victor Alejandro Venegas Villalobos
     * @since 20-SEP-2011
     */
    @Test
    public void testPresicionMaximaEnCampos() {
        ViewObject view = am.findViewObject("PrSegmentosCatastralesVO1");
        Row row = view.createRow();

        //TODO: Variables predefinidas con varias longitudes para
        //probar el m�ximo de caracteres permitidos en los campos de las tablas.

        String campo2000Chars = "";
        for (int i = 0; i < 2000; i++) {
            campo2000Chars += "1";
        }

        String campo100Chars = "";
        for (int i = 0; i < 100; i++) {
            campo100Chars += "1";
        }

        String campo50Chars = "";
        for (int i = 0; i < 50; i++) {
            campo50Chars += "1";
        }

        String campo15Chars = "";
        for (int i = 0; i < 15; i++) {
            campo15Chars += "1";
        }

        String campo10Chars = "";
        for (int i = 0; i < 10; i++) {
            campo10Chars += "1";
        }

        String campo4Chars = "";
        for (int i = 0; i < 4; i++) {
            campo4Chars += "1";
        }

        String campo2Chars = "";
        for (int i = 0; i < 2; i++) {
            campo2Chars += "1";
        }

        //TODO: Por cada atributo en la vista implementar lo siguiente:
        //NOTA: Los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Serie", Integer.parseInt(campo4Chars));
        row.setAttribute("Identificador", Long.parseLong(campo15Chars));
        row.setAttribute("ClaveSegmento", campo50Chars);
        row.setAttribute("Descripcion", campo100Chars);
        row.setAttribute("TipoSegmento", campo2Chars);
        row.setAttribute("Nivel", Long.parseLong(campo10Chars));
        row.setAttribute("Estatus", campo2Chars);
        row.setAttribute("CcRecaIdentificador", Long.parseLong(campo15Chars));
        row.setAttribute("PrSecaSerie", 2100);
        row.setAttribute("PrSecaIdentificador", 77777L);
        row.setAttribute("Campo1", campo2000Chars);
        row.setAttribute("Campo2", campo2000Chars);
        row.setAttribute("Campo3", campo2000Chars);
        row.setAttribute("Campo4", campo2000Chars);
        row.setAttribute("Campo5", campo2000Chars);
        row.setAttribute("Campo6", campo2000Chars);
        row.setAttribute("Campo7", campo2000Chars);
        row.setAttribute("Campo8", campo2000Chars);
        row.setAttribute("Campo9", campo2000Chars);
        row.setAttribute("Campo10", campo2000Chars);
        row.setAttribute("Campo11", campo2000Chars);
        row.setAttribute("Campo12", campo2000Chars);
        row.setAttribute("Campo13", campo2000Chars);
        row.setAttribute("Campo14", campo2000Chars);
        row.setAttribute("Campo15", campo2000Chars);
        //-------------------
        row.validate();
        am.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar altas y
     * bajas de registros en la vista de datos
     * se crea un nuevo row al cual se le llena con datos
     * de prueba para poder probar las operaciones de
     * insert y delete.
     *
     * @author: Victor Alejandro Venegas Villalobos
     * @since 20-SEP-2011
     */
    @Test
    public void testInsertUpdateDelete() {
        ViewObject view = am.findViewObject("PrSegmentosCatastralesVO1");
        Row row = view.createRow();

        //TODO: Llenar cada atributo de la vista con valores para probar los inserts y deletes
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.

        row.setAttribute("Serie", 2100);
        row.setAttribute("Identificador", 999999999999999L);
        row.setAttribute("ClaveSegmento", "CLAVE");
        row.setAttribute("Descripcion", "DESCRIPCION");
        row.setAttribute("TipoSegmento", "MU");
        row.setAttribute("Nivel", 1);
        row.setAttribute("Estatus", "AC");
        row.setAttribute("CcRecaIdentificador", 1);

        am.getDBTransaction().commit();
        //-------------------
        row.setAttribute("Nivel", 2);
        am.getDBTransaction().commit();
        //-------------------
        row.remove();
        am.getDBTransaction().commit();
    }

    @BeforeClass
    public static void setUp() {
        Statement stmt = am.getDBTransaction().createStatement(1);
        try {
            stmt.execute("Insert Into Pr_Segmentos_Catastrales (Serie,Identificador,Clave_Segmento,Descripcion,Tipo_Segmento,Nivel,Estatus,Cc_Reca_Identificador,Pr_Seca_Serie,Pr_Seca_Identificador,Campo1,Campo2,Campo3,Campo4,Campo5,Campo6,Campo7,Campo8,Campo9,Campo10,Campo11,Campo12,Campo13,Campo14,Campo15,Creado_Por,Creado_El,Modificado_Por,Modificado_El) values (2100,55555,'12345','JUNIT PRUEBA14','MU',1,'AC',1,NULL,NULL,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'00000','RECAUDADOR',to_date('15/09/11','DD/MM/RR'),'RECAUDADOR',to_date('19/09/11','DD/MM/RR'))");
            am.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            am.getDBTransaction().rollback();
        }
    }

    @AfterClass
    public static void tearDown() {
      Statement stmt = am.getDBTransaction().createStatement(1);
      try {
          stmt.execute("Delete from Pr_Segmentos_catastrales where Serie = 2100 and Identificador = 55555");
          am.getDBTransaction().commit();
      } catch (SQLException sqle) {
          sqle.printStackTrace();
          am.getDBTransaction().rollback();
      } 
    }
}