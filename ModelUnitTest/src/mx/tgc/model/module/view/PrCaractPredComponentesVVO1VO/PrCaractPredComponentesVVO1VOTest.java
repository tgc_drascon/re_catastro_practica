package mx.tgc.model.module.view.PrCaractPredComponentesVVO1VO;

import mx.tgc.model.module.applicationModule.ConsultaAMFixture;

import oracle.jbo.ViewObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class PrCaractPredComponentesVVO1VOTest {
    private ConsultaAMFixture fixture1 =
        ConsultaAMFixture.getInstance();

    public PrCaractPredComponentesVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrCaractPredComponentesVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}