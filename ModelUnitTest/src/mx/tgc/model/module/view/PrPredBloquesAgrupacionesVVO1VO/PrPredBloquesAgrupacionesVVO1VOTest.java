package mx.tgc.model.module.view.PrPredBloquesAgrupacionesVVO1VO;

import mx.tgc.model.module.applicationModule.CataBloquesPrediosAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrPredBloquesAgrupacionesVVO1VOTest {
    private CataBloquesPrediosAMFixture fixture1 = CataBloquesPrediosAMFixture.getInstance();

    public PrPredBloquesAgrupacionesVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PrPredBloquesAgrupacionesVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
