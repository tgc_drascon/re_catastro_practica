package mx.tgc.model.module.view.PrPrediosVVO1VO;

import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrPrediosVVO1VOTest {
    private CataSegCatastralesAMFixture fixture1 = CataSegCatastralesAMFixture.getInstance();

    public PrPrediosVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PrPrediosVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
