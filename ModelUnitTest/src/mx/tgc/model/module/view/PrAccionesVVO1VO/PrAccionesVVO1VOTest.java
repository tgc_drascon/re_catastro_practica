package mx.tgc.model.module.view.PrAccionesVVO1VO;

import mx.tgc.model.module.applicationModule.CatalogoSencilloAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrAccionesVVO1VOTest {
    private CatalogoSencilloAMFixture fixture1 = CatalogoSencilloAMFixture.getInstance();

    public PrAccionesVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PrAccionesVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
