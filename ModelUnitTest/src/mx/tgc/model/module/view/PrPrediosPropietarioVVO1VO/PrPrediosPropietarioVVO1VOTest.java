package mx.tgc.model.module.view.PrPrediosPropietarioVVO1VO;

import mx.tgc.model.module.applicationModule.ConsultaAMFixture;

import oracle.jbo.ViewObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class PrPrediosPropietarioVVO1VOTest {
    private ConsultaAMFixture fixture1 = ConsultaAMFixture.getInstance();

    public PrPrediosPropietarioVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrPrediosPropietarioVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}