package mx.tgc.model.module.view.PrTiposAvaluosVO1VO;

import mx.tgc.model.module.applicationModule.CataCaracteristicasValuacionAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrTiposAvaluosVO1VOTest {
    private CataCaracteristicasValuacionAMFixture fixture1 = CataCaracteristicasValuacionAMFixture.getInstance();

    public PrTiposAvaluosVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PrTiposAvaluosVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
