package mx.tgc.model.module.view.PrSegmentosCatastralesVO3VO;

import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrSegmentosCatastralesVO3VOTest {
    private CataSegCatastralesAMFixture fixture1 = CataSegCatastralesAMFixture.getInstance();

    public PrSegmentosCatastralesVO3VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PrSegmentosCatastralesVO3");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
