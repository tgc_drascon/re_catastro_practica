package mx.tgc.model.module;

import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;
import mx.tgc.model.module.applicationModule.CataSegCatastralesAMTest;
import mx.tgc.model.module.view.PcLocalidadesVVO1VO.PcLocalidadesVVO1VOTest;
import mx.tgc.model.module.view.PcMunicipiosRecaudacionesCustomVVO1VO.PcMunicipiosRecaudacionesCustomVVO1VOTest;
import mx.tgc.model.module.view.PrPrediosVVO1VO.PrPrediosVVO1VOTest;
import mx.tgc.model.module.view.PrSegmentosCatastralesUbicaVO1VO.PrSegmentosCatastralesUbicaVO1VOTest;
import mx.tgc.model.module.view.PrSegmentosCatastralesVO1VO.PrSegmentosCatastralesVO1VOTest;
import mx.tgc.model.module.view.PrSegmentosCatastralesVO2VO.PrSegmentosCatastralesVO2VOTest;
import mx.tgc.model.module.view.PrSegmentosCatastralesVO3VO.PrSegmentosCatastralesVO3VOTest;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { PrPrediosVVO1VOTest.class,
                       PrSegmentosCatastralesVO3VOTest.class,
                       PrSegmentosCatastralesUbicaVO1VOTest.class,
                       PrSegmentosCatastralesVO2VOTest.class,
                       PrSegmentosCatastralesVO1VOTest.class,
                       PcLocalidadesVVO1VOTest.class,
                       PcMunicipiosRecaudacionesCustomVVO1VOTest.class,
                       CataSegCatastralesAMTest.class })
public class AllCataSegCatastralesAMTests {
    @BeforeClass
    public static void setUp() {
    }

    @AfterClass
    public static void tearDown() throws Exception {
        CataSegCatastralesAMFixture.getInstance().release();
    }
}
