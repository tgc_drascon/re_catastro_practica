package mx.tgc.model.module.applicationModule;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class CataUsoSueloAMFixture {
    private static CataUsoSueloAMFixture fixture1 =
        new CataUsoSueloAMFixture();
    private ApplicationModule _am;

    private CataUsoSueloAMFixture() {
        _am =
Configuration.createRootApplicationModule("mx.tgc.model.module.CataUsoSueloAM",
                                          "CataUsoSueloAMLocal");
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public static CataUsoSueloAMFixture getInstance() {
        return fixture1;
    }

    public void release() throws Exception {
        Configuration.releaseRootApplicationModule(_am, true);
    }

    public ApplicationModule getApplicationModule() {
        return _am;
    }
}