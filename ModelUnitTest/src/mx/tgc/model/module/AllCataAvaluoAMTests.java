package mx.tgc.model.module;

import mx.tgc.model.module.applicationModule.CataAvaluoAMFixture;
import mx.tgc.model.module.applicationModule.CataAvaluoAMTest;
import mx.tgc.model.module.view.CcRecaudacionesVVO1VO.CcRecaudacionesVVO1VOTest;
import mx.tgc.model.module.view.PrPredialCaracteristicasVO1VO.PrPredialCaracteristicasVO1VOTest;
import mx.tgc.model.module.view.PrPredialValoresUnitariosVO1VO.PrPredialValoresUnitariosVO1VOTest;
import mx.tgc.model.module.view.PrTiposAvaluosVO1VO.PrTiposAvaluosVO1VOTest;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { PrPredialCaracteristicasVO1VOTest.class,
                       PrTiposAvaluosVO1VOTest.class,
                       CcRecaudacionesVVO1VOTest.class,
                       PrPredialValoresUnitariosVO1VOTest.class,
                       CataAvaluoAMTest.class })
public class AllCataAvaluoAMTests {
    @BeforeClass
    public static void setUp() {
    }

    @AfterClass
    public static void tearDown() throws Exception {
        CataAvaluoAMFixture.getInstance().release();
    }
}
