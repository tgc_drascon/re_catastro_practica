/**
 * Muestra una ventana para representar un mensaje.
 *
 */

function confirmaBorrado(mensaje)
{
    var borrar=confirm(mensaje);
    return borrar;
    
}

/**
 *Convierte las letras min�sculas a may�sculas.
 */

/*function aMayusc(campoTexto){
    alert ("entro");
    campoTexto.value = campoTexto.value.toUpperCase();
    alert("saldra");
     //var inputField = event.getSource();
     //inputField.setValue(event.getNewValue().toUpperCase());

 }*/
 
 function aMayusc(event){
     alert ("entro");
     var inputField = event.getSource();
     inputField.setValue(event.getNewValue().toUpperCase());

 }
 
function aMayuscTrim(objeto){
    aMayusc(objeto);
    Trim(objeto);
} 

/**
*
* Recibe el parametro y evalua que no contega caracteres especiales tales como:
* !@#$%^&*()+=-[]\\\';,./{}|:<>?
*@param campoTexto
*/
function aChars(campoTexto){ 
   var iChars = "!@#$^&*()+=-[]\\\';,./{}|:<>?";
  for (var i = 0; i < campoTexto.value.length; i++) {
  	if (iChars.indexOf(campoTexto.value.charAt(i)) != -1) {
            campoTexto.value = '';
            campoTexto.focus();
            alert ("No utilize Caracteres especiales \n como los son #!$%&/ etc...\n Por favor borrelos e intentelo de nuevo.");
            return false;
  	}
    }
}

//---------------------------------------------------------------------------//
//RTrim(string) : Returns a copy of a string without leading spaces. 
//by Brad Herder//
//---------------------------------------------------------------------------//
 function LTrim(str)
/*
   PURPOSE: Remove leading blanks from our string.
   IN: str - the string we want to LTrim
*/
{
   var whitespace = new String(" \t\n\r");

   var s = new String(str);

   if (whitespace.indexOf(s.charAt(0)) != -1) {
      // We have a string with leading blank(s)...

      var j=0, i = s.length;

      // Iterate from the far left of string until we
      // don't have any more whitespace...
      while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
         j++;

      // Get the substring from the first non-whitespace
      // character to the end of the string...
      s = s.substring(j, i);
   }
   return s;
}

//---------------------------------------------------------------------------//
//RTrim(string) : Returns a copy of a string without trailing spaces.        //
//by Brad Herder//
//---------------------------------------------------------------------------//
function RTrim(str)
/*
   PURPOSE: Remove trailing blanks from our string.
   IN: str - the string we want to RTrim

*/
{
   // We don't want to trip JUST spaces, but also tabs,
   // line feeds, etc.  Add anything else you want to
   // "trim" here in Whitespace
   var whitespace = new String(" \t\n\r");

   var s = new String(str);

   if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
      // We have a string with trailing blank(s)...

      var i = s.length - 1;       // Get length of string

      // Iterate from the far right of string until we
      // don't have any more whitespace...
      while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
         i--;


      // Get the substring from the front of the string to
      // where the last non-whitespace character is...
      s = s.substring(0, i+1);
   }

   return s;
}

//---------------------------------------------------------------------------//
//Trim(string) : Returns a copy of a string without leading or trailing spaces//
//---------------------------------------------------------------------------//

function Trim(objeto)
/*
   PURPOSE: Remove trailing and leading blanks from our string.
   IN: str - the string we want to Trim

   RETVAL: A Trimmed string!
*/
{  
   objeto.value = RTrim(LTrim(objeto.value));   
}

/**
Funci�n que revisa si un campo recibido como par�metro est� vacio y manda
un mensaje al usuario
@Author LHAR
@Date 10-Ene-2007
*/
function validaCampoVacio(campoTexto,mensaje)
{  
  var ret;
  if(campoTexto.value.length<1) {
       campoTexto.focus();
       alert (mensaje);
       ret = false;
  }
  else
  {
       ret = true;
  }
  return ret;
}


function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;

      }

//////////////////////////////////////////////////////
///////////METODOS PARA CONDONACION//////////////////
////////////////////////////////////////////////////
/** Este metodo recalcula toda la tabla, este metodo se ejecuta cuando se 
carga la pagina y cuando un dato es modificado. Es de uso interno
@autor: Andr�s Santana Martinez Garza
*/
function recalcular(input) {
    var renglon = input.renglon;
    var row =1;
    var a = new Array();
    var e;
    var i = 1;
    var porcientoNoPermitido = 0;
    var maximo = 0;
    var maxError = 0;
    var importeAux = 0;
    var total= 0;
    var acumulado=0;
    var totalDescuento = 0;
    var columna = 1;
    var ultimoAcomulado = 0;
    var descuentoGlobal;
    var descuentoGlobalAux = 0;
    var porcientoGlobal = new Number(0);
    var importeGlobal = new Number(0);
    var realizarSuma = false;
    var descuentoGlobalAdicionalAux = 0;
    var numeroDeAdicionales = 0;
    var numeroDeAdicionalesAux = 0;
    var descAdic = new Array();
    var numeroDeColumnas = new Array();
    var naturalezaContable = 'C';
    var abonos= new Number(0);
    var abonosGlobales = new Number(0);
    var diferencia = 0;
   // var contador = 1;
 //   var emtrar = 0;
   while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='hidden') || (e.type=='text')) {
            a.push (e);
        }
    }
    var importe = 0;
    i=1;
    calculaImporteAdicionalPorcentaje();
    ///AQUI METEMOS LA RECALCULACION DEL IMPORTE TOTAL DE LOS ADICIONALES
    for (var x = 0; x<a.length; x++) {
        if ((a[x].type=="text") || (a[x].type=="hidden"))  {
            var ind = a[x].id.lastIndexOf(':');
            var aux = a[x].id.substring(ind+1,a[x].id.length);
            var indAux = aux.lastIndexOf('_');
            if (indAux > 0 ) {
                aux = aux.substring(0,indAux);
            }
            if (aux=='naturaleza'){
                naturalezaContable = a[x].value;
            }
            if ((aux=='importe') && (naturalezaContable=='A')) {
                abonos=new Number(abonos) + new Number(a[x].value);
            }
            if ((aux=='totalAdicionalHidden') && (naturalezaContable=='A')) {
                abonosGlobales=new Number(abonosGlobales) + new Number(a[x].value);
            }
            if ((aux == 'porcentajemaximo') && (i==1)  ) {
                maximo = a[x].value;
                i=i+1;
            } 
            if (aux=='porcentajemaximoAdicional') {
                maximo = a[x].value;    
            }
            
            if ((aux=='importe') && (i==2)   ) {
                importe = new Number(a[x].value);
                //alert('IMPORTE MODIFICADO='+importe);
                i=i+1;
            }
            if ((aux=='importeAdiconales')  ) {
                importe = new Number(a[x].value);
                //alert('importeAdiconales='+importe);
            }  
            
            if ((aux=='descuento') && (i==3)){
            ////hay k validar
                var descuentoTemp = 0;
                if (validarFlotante(a[x])) {
                    importeAux = (a[x].value * 100)/importe;
                    if (importeAux>maximo) {
                        //importe = importe;
                        porcientoNoPermitido = 1;
                        maxError = maximo;
                        if (naturalezaContable=='C') {
                            total = new Number(total) + new Number(importe);
                            //alert('IMPORTE='+importe);
                        }
                        else {
                            total = new Number(total) - new Number(importe); 
                        }
                        if (columna==1) {
                            if (naturalezaContable=='C') {
                                totalDescuento = 
                                                new Number(totalDescuento) 
                                                + new Number(a[x].value);
                            }
                            else {
                                totalDescuento = 
                                                new Number(totalDescuento) 
                                                - new Number(a[x].value);                            
                            }
                            columna = 2;
                        }
                        a[x].value=0;
                    }
                    else {
                        if (naturalezaContable=='C') {
                            importe = new Number(importe) - new Number(a[x].value);
                            total = new Number(total) + new Number(importe);
                            descuentoTemp = new Number(a[x].value);
                        }
                        else {
                            importe = new Number(importe) + new Number(a[x].value);
                            total = new Number(total) - new Number(importe);
                            descuentoTemp = new Number(a[x].value);                            
                        }
                        if (columna==1) {
                            if (naturalezaContable=='C') {
                                totalDescuento = 
                                            new Number(totalDescuento) 
                                            + new Number(a[x].value);    
                            }
                            else {
                                totalDescuento = 
                                            new Number(totalDescuento) 
                                            - new Number(a[x].value);                            
                            }
                            columna = 2;
                        }
                    }
                    i=1;
                    importe= 0;     
                }
                if(validarPorciento(a[x])) {
                    var valorStr = a[x].value;
                    var desc = Number(valorStr.replace(',','').replace('%',''));
                    if ((desc>maximo)) {
                        importe = importe;
                        porcientoNoPermitido = 1;
                        maxError = maximo;
                        a[x].value=0;
                    }
                    else {
                        if (naturalezaContable=='C') {
                            descuentoTemp = importe*(desc/100);
                            var impAux = importe;
                            importe = (importe)-(importe*(desc/100));
                            total = new Number(total) + new Number(importe);
                            if (columna==1) {
                            totalDescuento = new Number(totalDescuento) 
                                                + new Number((impAux*desc)/100);
                                columna = 2;
                            }                        
                        }
                        else {
                            descuentoTemp = importe*(desc/100);
                            var impAux = importe;
                            importe = (importe)+(importe*(desc/100));
                            total = new Number(total) - new Number(importe);
                            if (columna==1) {
                            totalDescuento = new Number(totalDescuento) 
                                                - new Number((impAux*desc)/100);
                                columna = 2;
                            }                        
                        }
                    }
                    i=1;
                    importe= 0;
                }
                if (a[x].name>0) {
                    descAdic.push(descuentoTemp);
                    //numeroDeAdicionales = numeroDeAdicionales + 1;
                    var insertar = 0;
                    for (var z=0;z<numeroDeColumnas.length;z++) {
                        
                        if (a[x].name==numeroDeColumnas[z]) {
                            insertar = 1;
                        }
                    }
                    if (insertar==0) {
                        numeroDeColumnas.push(a[x].name);
                    }
                }
            } else {
                if ((maximo==0) && (i==3)  ) {
                    total = new Number(total) + new Number(importe);
                    //alert('NO DESCUENTO='+importe);
                    maximo = a[x].value;
                    i=1;
                }
            
            }
            if ((aux=='descuentoppal')) {
                porcientoGlobal =  new Number(maximo);
                importeGlobal = new Number(importe);
                descuentoGlobal = a[x];
                realizarSuma = !a[x].disabled;
                //////////////////////////entrar = 1;
                if (realizarSuma) {
                     if (validarFlotante(descuentoGlobal)) {
                        importeAux = 
                                (descuentoGlobal.value * 100)/importeGlobal;
                        if (importeAux>porcientoGlobal) {
                            //importe = importe;
                            porcientoNoPermitido = 1;
                            maxError = porcientoGlobal;
                        }
                        else {
                            descuentoGlobalAux = 
                                        new Number(descuentoGlobal.value);
                        }
                     }
                     //var cosa = validarPorciento(descuentoGlobal);
                     if(validarPorciento(descuentoGlobal)) {
                        var valorStr = descuentoGlobal.value;
                        var desc = 
                               Number(valorStr.replace(',','').replace('%',''));
                        importeAux = (importeGlobal * 100)/descuentoGlobal.value;
                        if ((desc>porcientoGlobal)) {
                            //importe = importe;
                            porcientoNoPermitido = 1;
                            maxError = maximo;
                        }
                        else {
                            //var impAux = importe;
                            descuentoGlobalAux = (importeGlobal*(desc/100));
                        }
                    }
                    //alert('no se que mas poner='+descuentoGlobalAux);
                }
            }
            ///ESTE IF ES PARA LOS ADICIONALES
            if (aux=='descuentoadiglobal') {
                //alert('aicional');
                var maximoAdicional = new Number(maximo);
                var importeAdicional = new Number(importe);
                var descuentoAdicional = a[x];
                if (!a[x].disabled) {
                ////////////
                     if (validarFlotante(descuentoAdicional)) {
                        importeAux = 
                              (descuentoAdicional.value * 100)/importeAdicional;
                        if (importeAux>maximoAdicional) {
                            //importe = importe;
                            porcientoNoPermitido = 1;
                            maxError = maximoAdicional;
                        }
                        else {
                            descuentoGlobalAdicionalAux = 
                                        descuentoGlobalAdicionalAux 
                                        + new Number(descuentoAdicional.value);
                        }
                     }
                     //var cosa = validarPorciento(descuentoGlobal);
                     if(validarPorciento(descuentoAdicional)) {
                        var valorStr = descuentoAdicional.value;
                        var desc = 
                              Number(valorStr.replace(',','').replace('%',''));
                        importeAux = 
                                (importeGlobal * 100)/descuentoAdicional.value;
                        if ((desc>maximoAdicional)) {
                            //importe = importe;
                            porcientoNoPermitido = 1;
                            maxError = maximo;
                        }
                        else {
                            descuentoGlobalAdicionalAux = 
                                        descuentoGlobalAdicionalAux 
                                        + (importeAdicional*(desc/100));
                        }                
    
                    }  
                } 
                else {////////operacion para el total del descuento adicional
                    var sumaTemporal = 0
                    //alert(numeroDeColumnas);
                    if (numeroDeColumnas.length==1) {
                        for (p=0;p<descAdic.length;p++) {
                            sumaTemporal = new Number(sumaTemporal) 
                                            + new Number(descAdic[p]);    
                            
                        }
                        a[x].value = new Number(sumaTemporal);
                
                    }
                    else {
                        if (descAdic.length>0) {
                            var inicio = new Number(a[x].name)- new Number(1);
                            var fin = new Number(descAdic.length);
                            var incremento =new Number(numeroDeColumnas.length);
                            for (var p=inicio;p<fin;p+=incremento) {

                                sumaTemporal = sumaTemporal + descAdic[p];
                            }
                            a[x].value = sumaTemporal;
                        }
                       
                    }
                }
            /////////////////////
            }
            if (aux=='diferencia') {
               diferencia = new Number(a[x].value);
            }
            ///////ESTE TOTAL ES PARA CADA RENGLON
            if ((aux=='total')  ) {
                //alert('total='+total);
                //alert('abonos='+abonos);
                var totalFixed = new Number(total) - new Number(abonos) ;
                a[x].value = totalFixed.toFixed(2);
                acumulado = new Number(acumulado) + new Number(total);
                total = 0;
                abonos = new Number(0);
                abonosGlobales = new Number(0);
                i=1;
            }
            if ((aux=='acumulado') ) {
                acumuladoFixed = new Number(acumulado);
                a[x].value = acumuladoFixed.toFixed(2);
                columna = 1;
            }
            if ((aux=='descuentoppal') ) {
                columna=1;
            }            
            if ((aux=='descuentoppal') && (row==renglon) ) {
                if (a[x].disabled==true) {
                    a[x].value = totalDescuento;
                    
                }
                columna=1;
            }
            if (aux=='final') {
                row = row+1;
                totalDescuento = 0;
                ////////////
                descuentoGlobalAdicionalAux = 0;
                numeroDeAdicionales = 0;
                numeroDeColumnas = new Array();
                descAdic = new Array();
                diferencia = 0;
                /////////
            }
            if (aux=='totalConcepto') { //TOTAL DEL CUADRITO DEL CONCEPTO
                if (realizarSuma) {
                    acumulado = acumulado - descuentoGlobalAux;
                    realizarSuma=false;
                    descuentoGlobalAux = 0;
                }
                //alert('dif'+diferencia);
                acumulado = acumulado - descuentoGlobalAdicionalAux - new Number(abonosGlobales) - diferencia;
                descuentoGlobalAdicionalAux = 0;
                var totalCFixed =  new Number(acumulado) - new Number(ultimoAcomulado); 
                a[x].value = totalCFixed.toFixed(2) ;
                //HAY QUE RESTARLE LA DIFERENCIA DE LOS TOTALES           
                importeGlobal = 0;
                ultimoAcomulado = new Number(ultimoAcomulado) + new Number(a[x].value);
                totalDescuento = 0;
                total = 0;
                i=1;
            }
            
            if (aux=='granTotal') {
                a[x].value = acumulado.toFixed(2);
                ///formating numeros
                
            }
        }
    }
    if (porcientoNoPermitido==1) {
        //EN caso dado de que se haya pasado del porciento permitido
        //alert('porcentaje no permitido');
        return maxError;   
    }
    else {
        //var variable = calcularImportesPrincipales(input);
        traspazarValores();
        return 0;
    }
}


///////////////////////////////////////
/**Este metodo lo usa el recalcular para sber si es porcentaje o cantidad
monetaria
@author Andres Santana Martinez Garza
*/
function validarPorciento(objeto) {
    var valorStr = objeto.value;
    var nombre = "";
    var eval = /^[\d]*[\.]?[\d]{1,2}[%]{1}$/;
    if (!eval.test(valorStr)) {
      return false;
    }
    return true; 

}

/**Este metodo lo usa el recalcular para sber si es porcentaje o cantidad
monetaria
@author Andres Santana Martinez Garza
*/
function validarFlotante(objeto){
  if(isNaN(objeto.value) || (objeto.value) < 0)
  {
    return false;
  }
  return true;
}

function validarEntero(objeto)
{ 
  var i;
  var abc= /[0-9]/;
  for(i=0;i<objeto.value.length;i++)
   {  bool=abc.test(objeto.value.charAt(i));
      if(bool);
      else
          {alert("Debe introducir un valor num�rico entero.");
           objeto.focus();
           objeto.select();
           objeto.value='';
           return false;
           break;}
         }
  return true;       
   
}

/**
Este metodo esta ligado a cada inputtext donde el usuario ingresa un descuento
para que valide que sea un flotante o una cantidad con signo de %
y a su vez manda llamar a recalcular para hacer los cambios correspondientes
@author Andres Santana Martinez Garza
**/
function validarDescuento(monto) {
    var valorStr = monto.value;
    var nombre = "";
    var eval = /^[\d]*[\.]?[\d]{1,2}[%]?$/;
    if (!eval.test(valorStr)) {
      alert('Debe introducir un formato de numero valido:\na) Puede ser' + 
            ' entero.\nb) Sin letras o comas.\nc) Dos decimales.\nd) Signo' +
            ' de % al final.');
      monto.focus();
      monto.select();
      monto.value="0"; 
      return false;
    }
    var aux = recalcular(monto);
    if (aux>0) {
      alert('Usted solo puede descontar maximo un '+aux+'%');
      monto.focus();
      monto.select();
      monto.value = "0";
      return false;
    }

}


/**
Esta funcion maneja los checkbox para habilitar y deshabilitar el inputtext
de enseguida y nuevamente realiza el calculo
@Andres Santana Martinez Garza
**/
function manejarCheck(check) {//checkbox
    var renglon = check.renglon;
    row = 1;
    var a = new Array();
    var e;
    var i = 0;
    var checkAux = check.checked;
    var entrar = 0;
    var elemento = check.elemento;
    var elementoAux = 1;
    //var value = 0;
    while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='checkbox')|| (e.type=='hidden') || (e.type=='text') ) {
            a.push (e);
        }
    }
    if (!checkAux) {
        //alert(a.length);
        for (var x = 0; x<a.length; x++) {
            var ind = a[x].id.lastIndexOf(':');
            var aux = a[x].id.substring(ind+1,a[x].id.length);
            var indAux = aux.lastIndexOf('_');
            if (indAux > 0 ) {
                aux = aux.substring(0,indAux);
            }
            
            if ((a[x].type=='text') &&(entrar==1)) {
                a[x].disabled=true;
                a[x].value='0';
                //a[x+1].value=value;
                entrar=0;            
            }
            if ((a[x].type=='checkbox') && (a[x].id==check.id) && (a[x].elemento==elemento)) {
                //value = a[x-2].value;

                entrar =1;    
            }
            if (aux=='total') {
                elementoAux = elementoAux + 1;
            }            
        }
        
    }
    if (checkAux) {
        //alert('Deschecado '+a.length);
         for (var x = 0; x<a.length; x++) {
            var ind = a[x].id.lastIndexOf(':');
            var aux = a[x].id.substring(ind+1,a[x].id.length);
            var indAux = aux.lastIndexOf('_');  
            if (indAux > 0 ) {
                aux = aux.substring(0,indAux);
            }            
            if ((a[x].type=='text') &&(entrar==1)) {            
                a[x].disabled=false;
                a[x].value='0';
                entrar=0;            
            }
            if ((a[x].type=='checkbox') && (a[x].id==check.id) && (a[x].elemento==elemento)) {
                //value = a[x-2].value;   
                
                entrar =1;    
            }
            if ((aux=='descuentoppal') && (row==renglon) && (check.id!='check2')) {
                a[x].disabled=true; 
                a[x].value='0';
            }
            if ((aux=='checkGlobal') && (row==renglon) && (check.id!='check2')) {
                a[x].checked=false;
            }
            
            if ((aux=='descuentoadiglobal') && (row==renglon) && (check.id=='check2')) {
                a[x].disabled=true; 
                a[x].value='0';
            }
            if ((aux=='checkAdicional') && (row==renglon) && (check.id=='check2')) {
                a[x].checked=false;
            }
            if (aux=='final') {
                row = row + 1;
                //elementoAux = 1;
            }
            if (aux=='total') {
                elementoAux = elementoAux + 1;
            }
        }
    }
    var aux = recalcular(check);
}

/**
@Andres Santana Martinez Garza
Este metodo imprime el total de los conceptos adicionales por cada obligacion
**/
function calcularImportesAdicionales(cont) {
    var contador = new Number(cont)+1;
    var a = new Array();
    var e;
    var suma = 0;
    var i=0;
    while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='hidden') || (e.type=='text') ) {
            a.push (e);
        }
    }
    i=new Number(0);
    var adic = 0;
    for (var x = 0; x<a.length; x++) {
      var ind = a[x].id.lastIndexOf(':');
      var aux = a[x].id.substring(ind+1,a[x].id.length);
      var indAux = aux.lastIndexOf('_');
      if (indAux > 0 ) {
          aux = aux.substring(0,indAux);
      }
      if ((aux=='porcentajemaximo') && (i==contador)) {
          if (a[x].value<1) {
              i=0;
          }
      }
      if (aux=='importe') {
          i=i+1;
      }
      if ((aux=='importe') && (i==contador)) {
          suma = new Number(suma) + new Number(a[x].value);
          i=-20;
      }
      if (aux=='acumulado') {
          i=0;
      }
      if (aux=='final') {
          suma = 0;
          i=0;
      }
    }
    return new Number(suma);
    ///
}


/**
Esta funcion deshabilitada toda la linea de checkbox principales, y en 
pocas palabras hace lo mismo que el manejarCheck()
@Andres Santana Martinez Garza
**/
function manejarCheckGeneral(check) {//checkbox
    var renglon = check.renglon;
    var a = new Array();
    var e;
    var i = 0;
    var checkAux = check.checked;
    var entrar = 2;
    var row = 1;
    //var value = 0;
    while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='checkbox')||  (e.type=='text') || (e.type=='hidden') ) {
            a.push (e);
        }
    }
    if (!checkAux) {
        //alert(a.length);
        for (var x = 0; x<a.length; x++) {
            var ind = a[x].id.lastIndexOf(':');
            var aux = a[x].id.substring(ind+1,a[x].id.length);
            var indAux = aux.lastIndexOf('_');
            if (indAux > 0 ) {
                aux = aux.substring(0,indAux);
            }
            
            if ((a[x].type=='text') && (entrar==1) && (row==renglon))  {
                a[x].disabled=false;
                a[x].value='0';
                //a[x+1].value=value;
                entrar=0;            
            }
            if ((a[x].type=='checkbox') && (entrar==2) && (aux!='checkGlobal') && (aux!='checkAdicional') && (row==renglon)) {
                //value = a[x-2].value;
                a[x].checked=true;
                entrar =1;    
            }
            if ((aux=='acumulado') && (row==renglon)) {
                entrar=2;

                
            }
            if ((aux=='descuentoppal') && (row==renglon)) {
                a[x].disabled=true;    
                a[x].value=0;
            }
            
            if (aux=='final') {
                row = row + 1;
            }
        }
    }
    if (checkAux) {
        //alert('Deschecado '+a.length);
         for (var x = 0; x<a.length; x++) {
            var ind = a[x].id.lastIndexOf(':');
            var aux = a[x].id.substring(ind+1,a[x].id.length);
            var indAux = aux.lastIndexOf('_');
            if (indAux > 0 ) {
                aux = aux.substring(0,indAux);
            }         
            if ((a[x].type=='text') &&(entrar==1) && (row==renglon)) {
                a[x].disabled=true;
                a[x].value='0';
                //a[x+1].value=value;
                entrar=0;            
            }
            if ((a[x].type=='checkbox') && (entrar==2) && (aux!='checkGlobal') && (aux!='checkAdicional') && (row==renglon)) {
                //value = a[x-2].value;
                a[x].checked=false;
                entrar =1;    
            }
            if ((aux=='acumulado') && (row==renglon)) {
                entrar=2;
                
            }
            if ((aux=='descuentoppal') && (row==renglon)) {
                a[x].disabled=false;    
                a[x].value=0;
            } 
            if (aux=='final') {
                row = row + 1;
            }            
        }
    }
    var aux = recalcular(check);
}

/**Este maneja los checkbox de los adicionales que deshabilita
toda la columna superior al seleccionado
@Andres Santana Martinez Garza
**/
function manejarCheckGeneralAdicionales(check) {//checkbox
    var cont = new Number(check.name);
    var renglon = new Number(check.renglon);
    //alert('este es el pinche separacion'+renglon);
    var row = 1;
    var a = new Array();
    var e;
    var i = 0;
    var checkAux = check.checked;
    var entrar = 0;
    var columna = 0;
    //var value = 0;
    while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='checkbox')||  (e.type=='text') || (e.type=='hidden') ) {
            a.push (e);
        }
    }
    if (!checkAux) {
        //alert(a.length);
        for (var x = 0; x<a.length; x++) {
            var ind = a[x].id.lastIndexOf(':');
            var aux = a[x].id.substring(ind+1,a[x].id.length);
            var indAux = aux.lastIndexOf('_');
            if (indAux > 0 ) {
                aux = aux.substring(0,indAux);
            }
            
            if ((a[x].type=='text') && (entrar==1))  {
                a[x].disabled=false;
                a[x].value='0';
                //a[x+1].value=value;
                entrar=0;            
            }
            if ((a[x].type=='checkbox') && (columna==cont) && (aux!='checkAdicional')  && (renglon==row)) {
                a[x].checked=true;
                entrar =1;    
            }
            if (a[x].type=='checkbox') {
                columna = columna + 1;
                //alert(columna);
            }            
            if (aux=='acumulado') {
                columna=0;
                
            }
          /*  if ((aux=='descuentoadiglobal')) {
                alert("rows"+a[x].renglon+','+row);
                alert("cols"+a[x].name+','+columna);
                
            }*/
            if ((aux=='descuentoadiglobal') && (a[x].renglon==row) && (a[x].name==cont) && (check.renglon==a[x].renglon)) {
                a[x].disabled=true;    
                a[x].value=0;
            }
            if (aux=='final') {
                row = row + 1;
                columna=0;
            }
        }
    }
    if (checkAux) {
        //alert('Deschecado '+a.length);
         for (var x = 0; x<a.length; x++) {
            var ind = a[x].id.lastIndexOf(':');
            var aux = a[x].id.substring(ind+1,a[x].id.length);
            var indAux = aux.lastIndexOf('_');
            if (indAux > 0 ) {
                aux = aux.substring(0,indAux);
            }         
            if ((a[x].type=='text') &&(entrar==1)) {
                a[x].disabled=true;
                a[x].value='0';
                //a[x+1].value=value;
                entrar=0;            
            }
            if ((a[x].type=='checkbox') && (columna==cont) && (aux!='checkAdicional') && (a[x].renglon==row) && (renglon==row)) {
                a[x].checked=false;
                columna=-100;
                entrar =1;    
            }
            if (a[x].type=='checkbox') {
                //alert('antes'+columna+' contador'+cont);
                columna = columna + 1;

            }   
            if (aux=='acumulado') {
                columna=0;
                
            }   
           /* if ((aux=='descuentoadiglobal')) {
                alert("rows"+a[x].renglon+','+row);
                alert("cols"+a[x].name+','+columna);
                
            }          */  
           /* if (aux=='descuentoadiglobal') {
                var auxCol = a[x].name -1  ;
                //alert('name del input='+auxCol+' name del check'+check.name);//ESTA ES LA RESP
            }*/
            if ((aux=='descuentoadiglobal') && (a[x].renglon==row) && (a[x].name==cont) && (check.renglon==a[x].renglon)){//&& (auxCol==check.name)) {
                a[x].disabled=false;    
                a[x].value=0;
            } 
            if (aux=='final') {
                row = row + 1;
                columna=0;
            }
        }
    }
    var aux = recalcular(check);
}

/**
Metodo que traspaza los valores que el usuario ingreso a los input hidden
que estan conectados al processScope y luego poder accesarlos desde un bean
@autor Andres Santana Martinez Garza
**/
function traspazarValores(){
//porcentajeAsignado
    var a = new Array();
    var e;
    var temporal = 0;
    var i = 1;
    var temporalGlobal = 0;
   while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='hidden') || (e.type=='text')) {
            a.push (e);
        }
    }
    for (var x = 0; x<a.length; x++) {
        var ind = a[x].id.lastIndexOf(':');
        var aux = a[x].id.substring(ind+1,a[x].id.length);
        var indAux = aux.lastIndexOf('_');
        if (indAux > 0 ) {
            aux = aux.substring(0,indAux);
        }
        if (aux=='descuento') {
            temporal = a[x].value;  
        }
        if (aux=='porcentajeAsignado') {
            a[x].value = temporal;
            temporal = 0;
        }
        
       if (aux=='descuentoppal') {
            if (!a[x].disabled) {
                temporalGlobal = a[x].value;
            }
        }
        if (aux=='porcentajeAsignadoPpal') {
           a[x].value=temporalGlobal;
           temporalGlobal = 0;
        }
        
        if (aux=='descuentoadiglobal') {
            if (!a[x].disabled) {
                temporalGlobal = a[x].value;
            }        
        
        }
        if (aux=='porcentajeAsignadoAdic') {
            a[x].value=temporalGlobal;
            temporalGlobal = 0;
            
            
        }
    }
            

}

function validaPlaca(campoTexto){ 
   var iChars = "%!@#$^&*()+=[]\\\';,./{}|:<>?";
  for (var i = 0; i < campoTexto.value.length; i++) {
  	if (iChars.indexOf(campoTexto.value.charAt(i)) != -1) {
            campoTexto.value = '';
            campoTexto.focus();
            alert ("No utilize Caracteres especiales \n como los son #!$%&/ etc...\n Por favor borrelos e intentelo de nuevo.");
            return false;
  	}
    }
    aMayuscTrim(campoTexto);
}



function recalcularVehicular(input) {
    var a = new Array();
    var e;
    var i = 1;
    var max = 0;
    var entrar = 0;
    var descuento = 0;
    var subsidio = 0;
    var totalAux = 0;
    var porcentajeAux = 0;
    var error = 0;
    var acumulado = 0;
    var maxError = 0;
   while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='hidden') || (e.type=='text')) {
            a.push (e);
        }
    }
    var importe = 0;
    for (var x = 0; x<a.length; x++) {
        if ((a[x].type=="text") || (a[x].type=="hidden"))  {
            var ind = a[x].id.lastIndexOf(':');
            var aux = a[x].id.substring(ind+1,a[x].id.length);
            var indAux = aux.lastIndexOf('_');
            if (indAux > 0 ) {
                aux = aux.substring(0,indAux);
            }
            if (aux=='importe'){
                importe = new Number(a[x].value);
            }
            if (aux=='max'){
                max = new Number(a[x].value);
            }
            if (aux=='descuento') {
                descuento = new Number(a[x].value);
/////////////////
                if (validarFlotante(a[x])) { 
                    porcentajeAux = (new Number(descuento) * 100)/ new Number(importe);
                    if (porcentajeAux>max) {
                        a[x].value = '0';
                        error = 1;
                        maxError = max;
                        totalAux = importe;
                    }
                    else {
                        totalAux = importe - descuento;
                    }
                }
                if(validarPorciento(a[x])) {
                    var valorStr = a[x].value;
                    var desc = Number(valorStr.replace(',','').replace('%',''));
                    if ((desc>max)) {
                        a[x].value = '0';
                        error = 1;
                        maxError = max;
                        totalAux = importe;
                    }
                    else {
                       totalAux = importe - (importe*(desc/100));                    
                    }

                }
            }
            if (aux=='subsidio') {
                subsidio = new Number(a[x].value);
/////////////////
                if (validarFlotante(a[x])) { 
                    porcentajeAux = (new Number(subsidio) * 100)/ new Number(importe);
                    if (porcentajeAux>max) {
                        a[x].value = '0';
                        error = 1;
                        maxError = max;
                        totalAux = importe;
                    }
                    else {
                        totalAux = importe;
                    }
                }
                if(validarPorciento(a[x])) {
                    var valorStr = a[x].value;
                    var desc = Number(valorStr.replace(',','').replace('%',''));
                    if ((desc>max)) {
                        a[x].value = '0';
                        error = 1;
                        maxError = max;
                        totalAux = importe;
                    }
                    else {
                       totalAux = (importe*(desc/100));                    
                    }

                }
            }
            
            if (aux=='total') {
                if (max>0) {
                    var redondeo = new Number(totalAux);
                    var totalFixed =  Math.round(redondeo*100)/100;  
                    a[x].value = totalFixed.toFixed(2);
                }
                else {
                    var redondeo = new Number(importe)
                    var totalFixed =  Math.round(redondeo*100)/100;  
                    a[x].value = totalFixed.toFixed(2);
                }
                totalAux = 0;
                acumulado = new Number(acumulado) +  new Number(a[x].value);
            }
            if (aux=='acumulado'){
                var redondeo = new Number(acumulado);
                var totalFixed = Math.round(redondeo*100)/100;  
                a[x].value = totalFixed.toFixed(2);
            }
            if (aux=='totalGlobal'){
                var redondeo2 = new Number(acumulado);
                var totalFixed = Math.round(redondeo2*100)/100;  
                a[x].value = totalFixed.toFixed(2);
            }
        }
    }
    if (error>0) {
        return maxError;
    }
    return error;
}


function validarDescuentoVehicular(monto) {
    var valorStr = monto.value;
    var nombre = "";
    var eval = /^[\d]*[\.]?[\d]{1,2}[%]?$/;
    if (!eval.test(valorStr)) {
      alert('Debe introducir un formato de numero valido:\na) Puede ser' + 
            ' entero.\nb) Sin letras o comas.\nc) Dos decimales.\nd) Signo' +
            ' de % al final.');
      monto.focus();
      monto.select();
      monto.value="0"; 
      return false;
    }
    var aux = recalcularVehicular(monto);
    if (aux>0) {
      alert('Usted solo puede descontar maximo un '+aux+'%');
      monto.focus();
      monto.select();
      monto.value = "0";
      return false;
    }

}

function reiniciarValores(objeto){
    var a = new Array();
    var e;
    var i = 1;
    while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='text')) {
            a.push (e);
        }
    }
    var importe = 0;
    for (var x = 0; x<a.length; x++) {
        var ind = a[x].id.lastIndexOf(':');
        var aux = a[x].id.substring(ind+1,a[x].id.length);
        var indAux = aux.lastIndexOf('_');
        if (indAux > 0 ) {
            aux = aux.substring(0,indAux);
        }
        if (aux=='descuento'){
            a[x].value='0';
        }
    }
    recalcularVehicular(objeto);
}
 
 function validarNumeroFlotante(objeto)
{
  if(isNaN(objeto.value))
     {alert("Esta incorrecto el campo porcentaje. Debe introducir un valor num�rico decimal.");
      objeto.focus();
      objeto.select();
      objeto.value='';
      return false;}
  return true;
  
}

function validarPorcentaje(objeto)
{
  if(parseFloat(objeto.value)>100)
     {alert("Esta incorrecto el campo porcentaje. El porcentaje no puede ser mayor a 100");
      objeto.focus();
      objeto.select();
      objeto.value='';
      return false;}
  return true;
  
}

function validarPorcentajeNegativo(objeto)
{
  if(parseFloat(objeto.value)<0)
     {alert("Esta incorrecto el campo porcentaje. El porcentaje no puede ser negativo");
      objeto.focus();
      objeto.select();
      objeto.value='';
      return false;}
  return true;
  
}

function validarFecha(mask,objeto){ 
if(objeto.value!="")
 {
  if(validarFormato(mask,objeto))
   {
    var ban=0;
    var dd = objeto.value.charAt(0)+objeto.value.charAt(1);
    var mm = objeto.value.charAt(3)+objeto.value.charAt(4);
    var aa = objeto.value.charAt(6)+objeto.value.charAt(7)+objeto.value.charAt(8)+objeto.value.charAt(9);

   //Checar los rangos de dia, mes y a�o
   if(dd>0 && dd<32 && mm>0 && mm<13 && aa>1900 && aa<2100)
   {
     //Si es abril, junio, septiembre, noviembre
      if(mm=="04"||mm=="06"||mm=="09"||mm=="11")
        if(dd>30)
         ban=1;
     //Checar si es febrero y a�o bisiesto
      if(mm==02)
        {if(bisiesto(aa))
          {if(dd>29)
            ban=1;}
          else
            if(dd>28)
              ban=1;}
         
     }
     else
       ban=1;
     if(ban==1)
           {alert("Esta incorrecto el campo. Debe introducir una fecha con formato dd-mm-aaaa.");
            objeto.focus();
            return false;}
     return true;       
            
  }
  }     
 }

function validarFormato(mask,objeto)
{
        var i;
        var ban=0;
        if(mask.length!=objeto.value.length)
        ban=1;
        else{
            for(i=0;i<objeto.value.length;i++){
                if(mask.charAt(i)=='#'){
                    if(!parseInt(objeto.value.charAt(i))&&objeto.value.charAt(i)!=0){
                        ban=1;
                        break;
                    }
                }else if(mask.charAt(i)!=objeto.value.charAt(i)){
                        ban=1;
                        break;
                }
            }
        }
        
        if(ban==1){
            alert("Esta incorrecto el campo: el formato debe ser dd-mm-aaaa");
            objeto.focus();
            objeto.select();
            objeto.value='';
            return false;
        }
        return true;

}


function validarAlfanumericaPura(objeto)
{ var i;
  var bool;
  var abc= /[a-zA-Z,�,�,\s,0-9,�,�,�,�,�,�,�,�,�,�,_,-,%]/;
  for(i=0;i<objeto.value.length;i++)
   { bool=abc.test(objeto.value.charAt(i));
     if(bool);
     else
       {alert("Debe introducir un valor alfanum�rico.");
         objeto.focus();
         objeto.value='';
         return false;
         break;}
         }
   return true;      
     
}

function validarAlfanumerica(objeto)
{ var i;
  var bool;
  var abc= /[a-zA-Z,�,�,\s,0-9,�,�,�,�,�,�,�,�,�,�,_,-]/;
  for(i=0;i<objeto.value.length;i++)
   { bool=abc.test(objeto.value.charAt(i));
     if(bool);
     else
       {alert("Debe introducir un valor alfanum�rico.");
         objeto.focus();
         objeto.value='';
         return false;
         break;}
         }
   return true;      
     
}

function validarPlaca(objeto)
{ var i;
  var bool;
  var abc= /[a-zA-Z,�,�,\s,0-9]/;
  for(i=0;i<objeto.value.length;i++)
   { bool=abc.test(objeto.value.charAt(i));
     if(bool);
     else
       {alert("Debe introducir un valor de placa valida.");
         objeto.focus();
         objeto.value='';
         return false;
         break;}
         }
   return true;      
     
}

function reiniciarValoresMultas(){
//porcentajeAsignado
    var a = new Array();
    var e;
    var temporal = 0;
    var i = 1;
    var temporalGlobal = 0;
   while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='hidden') || (e.type=='text')) {
            a.push (e);
        }
    }
    for (var x = 0; x<a.length; x++) {
        var ind = a[x].id.lastIndexOf(':');
        var aux = a[x].id.substring(ind+1,a[x].id.length);
        var indAux = aux.lastIndexOf('_');
        if (indAux > 0 ) {
            aux = aux.substring(0,indAux);
        }
        if (aux=='descuento') {
            a[x].value = '0.00';  
        }
        if (aux=='porcentajeAsignado') {
            a[x].value = '0.00'; 
        }
        
       if (aux=='descuentoppal') {
            a[x].value = '0.00'; 
        }
        if (aux=='porcentajeAsignadoPpal') {
            a[x].value = '0.00'; 
        }
        
        if (aux=='descuentoadiglobal') {
            a[x].value = '0.00';        
        }
        if (aux=='porcentajeAsignadoAdic') {
            a[x].value = '0.00'; 
              
        }
    }  

}


function validarAlfa(objeto)
{ var i;
  var abc= /[a-zA-Z,�,�,\s,\.,%,�,�,�,�,�,�,�,�,�,�]/;
  
  for(i=0;i<objeto.value.length;i++)
    { bool=abc.test(objeto.value.charAt(i));
      if(bool);
      else
        {
         alert("Debe introducir un valor alfab�tico.");
         objeto.value = '';
         objeto.focus();
         return false;
         break;
         }
         }
    return true;     
 }


/*funcion que obtiene un objeto fecha y lo convierte al formato dd-mm-aaaa
  en el caso en que el valor llegue ddmmaaaa o dd%mm%aaaa, si no es ninguno 
  de estos formatos muestra un mensaje de error
*/
function convertirFecha(objeto){
 var dd = "";
 var mm = "";
 var aa = "";
 if (objeto.value != '')
    if (objeto.value.length  != 10) {
        if (objeto.value.length == 8)    {
            dd = objeto.value.charAt(0)+objeto.value.charAt(1);
            mm = objeto.value.charAt(2)+objeto.value.charAt(3);
            aaaa = objeto.value.charAt(4)+objeto.value.charAt(5)+
                                  objeto.value.charAt(6)+objeto.value.charAt(7);            
            objeto.value = dd + '-' + mm + '-' + aaaa;
            return false;
        }
    }
    else {
        dd = objeto.value.charAt(0)+objeto.value.charAt(1);
        mm = objeto.value.charAt(3)+objeto.value.charAt(4);
        aaaa = objeto.value.charAt(6)+objeto.value.charAt(7)+
                                  objeto.value.charAt(8)+objeto.value.charAt(9);
        objeto.value = dd + '-' + mm + '-' + aaaa;
        return false;
    }    
}

function loadPopup(url){
    openWindow(window, url,'popUpWindow',{width:800, height:600});
    return false;
}

function validarEntero(objeto)
{ 
  var i;
  var abc= /[0-9]/;
  for(i=0;i<objeto.value.length;i++)
   {  bool=abc.test(objeto.value.charAt(i));
      if(bool);
      else
          {alert("Debe introducir un valor num�rico entero.");
           objeto.focus();
           objeto.select();
           objeto.value='';
           return false;
           break;}
         }
  return true;       
}

function calculaImporteAdicionalPorcentaje(){
   var a = new Array();
   var e;
   var i = 1;
   var entrar = false;
   while (e = document.getElementsByTagName ('*')[i++]){
      if ((e.type=='hidden') || (e.type=='text')) {
         a.push (e);
      }
   }
   var importe = 0;
   var descuento = 0;
   var totalPadre = 0;
   var esPorcentaje = false;
   var totalAdicional = 0;
   var importeAnterior = 0;
   var posicion = 0;
   var posicionDiferencia = 0;
   var seHizoRecalculo = false;
   for (var x = 0; x<a.length; x++) {
      var ind = a[x].id.lastIndexOf(':');
      var aux = a[x].id.substring(ind+1,a[x].id.length);
      var indAux = aux.lastIndexOf('_');
      if (indAux > 0 ) {
         aux = aux.substring(0,indAux);
      }
      if (aux=='marcaInicio'){    
         //Estamos en el padre entonces obtenemos el importe y el descuento
         //alert('inicio');
         entrar = true;
         importe = 0;
         descuento = 0;
         totalPadre = 0;     
         totalAdicional = 0;
      }
      if (aux=='importe' && entrar==true) {
         importe = new Number(a[x].value); 
         totalPadre = importe;
      }
      if (aux=='importe' && entrar==false){
         importeAnterior = new Number(a[x].value);
         posicion = x;
      }
      if (aux=='importeAdiconales' && entrar==false) {
         posicion = x;
      }
      if (aux=='descuento' && entrar==true){
         //alert('descuento'+a[x].value);
            if(validarPorciento(a[x])) {
               //si es porciento hay que ver como rempplezamos los signos de %
               var valorStr = a[x].value;
               var desc = Number(valorStr.replace(',','').replace('%',''));
               totalPadre = importe-importe*(desc/100);
               //Sobre descuentoTemp ya sabemos lo que nos quedo del principal
            }
            else {
               //es por que mas bien es cantidad
               var valorLong = a[x].value;           
               totalPadre = importe - valorLong
            }
         //alert('totalPadre'+totalPadre);
         entrar=false;
      }
      else if (aux=='descuentoppal' && entrar==true) {
         //alert('que paso');
            if(validarPorciento(a[x])) {
               //si es porciento hay que ver como rempplezamos los signos de %
               var valorStr = a[x].value;
               var desc = Number(valorStr.replace(',','').replace('%',''));
               totalPadre = importe-importe*(desc/100);
               //Sobre descuentoTemp ya sabemos lo que nos quedo del principal
            }
            else {
               //es por que mas bien es cantidad
               var valorLong = a[x].value;
               totalPadre = importe - valorLong;
            }
         //alert('totalPadre'+totalPadre);
         entrar=false;      
      }
      if (aux=='tipo') {
         //alert('tipo');
         var tipo = a[x].value;
         //alert('tipo'+tipo);
         if (tipo=='P') {
            //Se recalcula el adicional
            esPorcentaje = true;
            seHizoRecalculo = true;
         }
         else {
            esPorcentaje = false;
            seHizoRecalculo = false;         
         }
      }
      if (aux=='porcentajeTarifa' && esPorcentaje==true) {
         //alert('porcentajeTarifa'+a[x].value);
         //Hacemos el calculo
         var porcentajeAux = a[x].value;
         totalAdicional = totalPadre * (porcentajeAux/100);
         //alert('POR'+totalPadre);
         //alert('porcentajeAux='+porcentajeAux);
         //alert('totalPadre='+totalPadre);
         //alert('found1='+totalAdicional);
      }
      if (aux=='importeAdiconales' && esPorcentaje==true) {
         a[x].value=totalAdicional.toFixed(2);
      }
      if (aux=='importeCalculado' && esPorcentaje==true) {
         //alert('importeCalculado');
         //alert('importeanterior'+importeAnterior);
         //alert('posicion'+posicion);
         //alert('valoract'+a[x].value);
         //alert('found='+totalAdicional);
         if (importeAnterior==a[x].value) {
            a[posicion].value=totalAdicional.toFixed(2);
         }
         a[x].value=totalAdicional.toFixed(2);
         esPorcentaje=false;
      }
      if (aux=='diferencia') {
         posicionDiferencia = x;
      }
      if (aux=='totalAdicionalHidden'){
        // if (importeAnterior==a[x].value) {
            //alert('found'+totalAdicional);
          //  a[posicion].value=totalAdicional;
         //}
         //a[x].value=totalAdicional;
         //alert('totalAdicional'+totalAdicional);
         //a[x].value = recalcularImportesAdicionales(a[x].contador);
         if (seHizoRecalculo==false){
            totalAdicional = a[x].value;
            //alert('recalculo false');
            //alert(a[x].value);
         }
         else {
            //alert('recalculo true');
            seHizoRecalculo ==false;
         }
         var calculo = recalcularImportesAdicionales(a[x].contador);
         //alert('totaladuc='+totalAdicional);
         var diferencia = totalAdicional-calculo;
         //alert('calculo='+calculo);
         //alert('eh='+a[posicion].value);
         //alert('wtf'+totalAdicional);
         //alert('shut');
         //a[posicion].value=totalAdicional;
         //alert('dif'+diferencia);
         //alert('totalAdicional'+totalAdicional);
         var totalAdicionalAux = 0;
         if (diferencia>0) {
            totalAdicionalAux = new Number(totalAdicional) - new Number(diferencia);
         }
         else {
            //totalAdicional = 1;
            totalAdicionalAux = new Number(totalAdicional);
            //alert('totalAdicionalAux='+totalAdicionalAux);
            //alert('importeAnterior='+importeAnterior);
            var diferenciaAux = new Number(importeAnterior) - new Number(totalAdicionalAux);
            //alert('diferenciaAux='+diferenciaAux);
            if (diferenciaAux>0) {
               a[posicionDiferencia].value = diferenciaAux.toFixed(2);
            }
            else {
               a[posicionDiferencia].value = 0;
            }
            //alert(a[posicionDiferencia].value);
         }
         a[x].value = totalAdicionalAux.toFixed(2);
         //alert('TOTAL='+a[x].value);
      }
   }
   //calculaImporteTotalAdicionalesPorPorcentaje();
}

/**
@Andres Santana Martinez Garza
Este metodo imprime el total de los conceptos adicionales por cada obligacion
**/
function recalcularImportesAdicionales(cont) {
    var contador = 1;
    //alert('cont='+cont);
    var a = new Array();
    var e;
    var suma = 0;
    var i=0;
    while (e = document.getElementsByTagName ('*')[i++]){
        if ((e.type=='hidden') || (e.type=='text') ) {
            a.push (e);
        }
    }
    i=new Number(0);
    var adic = 0;
    for (var x = 0; x<a.length; x++) {
      var ind = a[x].id.lastIndexOf(':');
      var aux = a[x].id.substring(ind+1,a[x].id.length);
      var indAux = aux.lastIndexOf('_');
      if (indAux > 0 ) {
          aux = aux.substring(0,indAux);
      }
      //Pero antes de asignarle la suma obtenemos al padre
      ///ahora si continua la suma
      if (aux=='topeAdicionales'){
         suma= 0;
         //contador = 1;
         //alert('tope'); 
      }
      if (aux=='importeCalculado' && cont==contador) {
         suma = new Number(a[x].value.replace(',','')) + suma;
         //contador +=1;
         return new Number(suma.toFixed(2));
      }
      else if (aux=='importeCalculado') {
         contador = contador + 1;
         //alert('sumando contador='+contador);
      }
    }
    return new Number(0);
    ///
}

function validarNombre(objeto)
{ var i;
  var abc= /[a-zA-Z,�,�,\s,&,�,�,�,�,�,�,�,�,�,�,\'']/;
  
  for(i=0;i<objeto.value.length;i++)
    { bool=abc.test(objeto.value.charAt(i));
      if(bool);
      else
        {
         alert("Debe introducir un valor alfab�tico.");
         objeto.value = '';
         objeto.focus();
         return false;
         break;
         }
         }
    return true;     
 }

function validarNombreBusq(objeto){ 
   var i;
   var abc= /[a-zA-Z,�,�,\s,\'',&,�,�,�,�,�,�,�,�,�,�,%]/;
   for(i=0;i<objeto.value.length;i++){ 
      bool=abc.test(objeto.value.charAt(i));
      if(bool){
         var apostrofe = /[\'']/;
         bool=apostrofe.test(objeto.value.charAt(i));
         if (bool){
            alert("En las consultas de la aplicaci�n, no es posible utilizar\n"+ 
               "el caract�r ap�strofe (\'), utilice el porciento (%) en su lugar.");
            objeto.value = '';
            objeto.focus();
            return false;
            break;            
         }
      }
      else{
         alert("Debe introducir un valor alfab�tico.");
         objeto.value = '';
         objeto.focus();
         return false;
         break;
      }
   }
   return true;     
 }
 
function validarEnterosNoEsponenciales(monto) {
    var valorStr = monto.value;
    var nombre = "";
    var eval = /^[\d]*[\.]?[\d]{1,2}$/;
    if (!eval.test(valorStr)) {
      if (valorStr!=''){
         alert('Debe introducir un formato de numero valido:\na) Puede ser' + 
            ' entero.\nb) Sin letras o comas.\nc) Dos decimales.\n');
         monto.focus();
         monto.select();
         monto.value=""; 
         return false;
      }
    }
    var aux = recalcularVehicular(monto);
    if (aux>0) {
      alert('Usted solo puede descontar maximo un '+aux+'%');
      monto.focus();
      monto.select();
      monto.value = "0";
      return false;
    }

}

// Funcion para validar estructura del campo RFC 
// agrega guiones en caso de que el usuario no los ingrese

function checaRFC(campoRFC)
{				
	var texto = campoRFC.value;
        if (texto!=""){
	var rfcGuion = "";	
	if (texto.length == 13){
        rfcGuion = validaPersona(/[a-zA-Z]{4}\d{6}\w{3}/,texto,false);
        }else if (texto.length == 12){	
        rfcGuion = validaPersona(/[a-zA-Z]{3}\d{6}\w{3}/,texto,true);
        }else if (texto.length == 15){
        rfcGuion = validaPersona(/[a-zA-Z]{4}\-\d{6}\-\w{3}/,texto,false);
        }else if (texto.length == 14){
        rfcGuion = validaPersona(/[a-zA-Z]{3}\-\d{6}\-\w{3}/,texto,true);
	}else if (texto.length >15){
        rfcGuion ="";
        }
          
        if(rfcGuion==""){
        alert("El formato de RFC que ingres� es inv�lido. Ingrese uno v�lido. \n" +
                                "Ejemplo: AAAA-782501-H13 � CCC-982208-A12");
        campoRFC.focus();
        campoRFC.value="";
        }else
         campoRFC.value = rfcGuion.toUpperCase();
        } 
}	

function validaPersona(rfcRegExp,texto,personaMoral){
    var rfcGuion = "";
	if (rfcRegExp.test(texto)){ 			
            
            var porcion = texto.split("-");			
            if(porcion.length > 1 )			   
            return texto;			
            
            if(personaMoral){				
                var p1 = texto.substring(0,3);				
                var p2 = texto.substring(3,9);
                var p3 = texto.substring(9,13);		
                rfcGuion=   p1+"-"+p2+"-"+p3;					
            }else
                {
                var pt1 = texto.substring(0,4);
                var pt2 = texto.substring(4,10);
                var pt3 = texto.substring(10,14);
                rfcGuion  = pt1+"-"+pt2+"-"+pt3;				
                 }	
          
        }
     return rfcGuion;                 
}




/*
function calculaImporteTotalAdicionalesPorPorcentaje(){
// HACER EL METODO PARA LA RECALCULACION DEL MALDITO TOTAL
//////////////////////////////////////////////////
   var a = new Array();
   var e;
   var i = 1;
   var entrar = false;
   while (e = document.getElementsByTagName ('*')[i++]){
      if ((e.type=='hidden') || (e.type=='text')) {
         a.push (e);
      }
   }
   var importe = 0;
   var descuento = 0;
   var totalPadre = 0;
   var esPorcentaje = false;
   var totalAdicional = 0;
   var importeAnterior = 0;
   var totalConcepto = 0;
   for (var x = 0; x<a.length; x++) {
      var ind = a[x].id.lastIndexOf(':');
      var aux = a[x].id.substring(ind+1,a[x].id.length);
      var indAux = aux.lastIndexOf('_');
      if (indAux > 0 ) {
         aux = aux.substring(0,indAux);
      }
      if (aux=='marcaInicio'){    
         //Estamos en el padre entonces obtenemos el importe y el descuento
         //alert('inicio');
         entrar = true;
         importe = 0;
         descuento = 0;
         totalPadre = 0;         
      }
      if (aux=='importe' && entrar==true) {
         importe = new Number(a[x].value); 
         //alert('importe'+a[x].value);
      }
      if (aux=='importe' && entrar==false){
         importeAnterior = new Number(a[x].value);
      }
      if (aux=='descuento' && entrar==true){
         //alert('descuento'+a[x].value);
            if(validarPorciento(a[x])) {
               //si es porciento hay que ver como rempplezamos los signos de %
               var valorStr = a[x].value;
               var desc = new Number(valorStr.replace(',','').replace('%',''));
               totalPadre = importe-importe*(desc/100);
               //Sobre descuentoTemp ya sabemos lo que nos quedo del principal
            }
            else {
               //es por que mas bien es cantidad
               var valorLong = new Number(a[x].value);
               totalPadre = importe - valorLong;
            }
         entrar=false;
      }
      else if (aux=='descuentoppal' && entrar==true) {
            if(validarPorciento(a[x])) {
               //si es porciento hay que ver como rempplezamos los signos de %
               var valorStr = a[x].value;
               var desc = new Number(valorStr.replace(',','').replace('%',''));
               totalPadre = importe-importe*(desc/100);
               //Sobre descuentoTemp ya sabemos lo que nos quedo del principal
            }
            else {
               //es por que mas bien es cantidad
               var valorLong = new Number(a[x].value);
               totalPadre = importe - valorLong;
            }
         entrar=false;      
      }
      if (aux=='tipo') {
         var tipo = a[x].value;
         if (tipo=='P') {
            esPorcentaje = true;
         }
      }
      //
      if (aux=='importeAdicionales' && esPorcentaje==true) {
         //alert('totalPadre='+totalPadre);
         //alert('a[x].value='+a[x].value);
         totalConcepto = new Number(totalPadre) + new Number(a[x].value);
      }
      else if (aux=='importeAdicionales'){
         //alert('importeAnterior='+importeAnterior);
         //alert('a[x].value='+a[x].value);
         totalConcepto = new Number(importeAnterior) + new Number(a[x].value);
      }
      if (aux=='totalConcepto'){
         alert('mergas'+totalConcepto);
         a[x].value=totalConcepto;
      }
   }
////////////////////////////////////////////////////
}*/

/**
*  Transfiere el foco de un componente al siguiente cuando se terminaron de
*  llenar el maximo numero de caracteres permitido.
*/

function toNextFocus(element,frm,nextElement)
{
 if (window.event.keyCode != 9 && window.event.keyCode != 16) {
   if (element.value.length==element.maxLength) {
      document.getElementById(frm + ':' + nextElement).focus();
   }
 } 
}
