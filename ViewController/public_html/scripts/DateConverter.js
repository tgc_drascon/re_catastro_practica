function DateConverter() {
    // for debugging
    this._class = "DateConverter";
}

DateConverter.prototype = new TrConverter();

DateConverter.prototype.getFormatHint = function () {
    return "16-01-1986";
}

DateConverter.prototype.getAsString = function (string, label) {
    var dia = string.value.charAt(0) + string.value.charAt(1);
    var mes = string.value.charAt(3) + string.value.charAt(4);

    var anio = string.value.charAt(6) + string.value.charAt(7) + string.value.charAt(8) + string.value.charAt(9);
    var cadena = dia + "-" + mes + "-" + anio;
    return cadena;
}

DateConverter.prototype.getAsObject = function (string, label) {
    var dia = string.value.charAt(0) + string.value.charAt(1);
    var mes = string.value.charAt(3) + string.value.charAt(4);

    var anio = string.value.charAt(6) + string.value.charAt(7) + string.value.charAt(8) + string.value.charAt(9);
    var cadena = dia + "-" + mes + "-" + anio;
    return cadena;
}