package mx.tgc.view.backing.catalogo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.net.tgc.recaudador.resource.pc.Accion;

import mx.net.tgc.recaudador.resource.pr.Elemento;
import mx.net.tgc.recaudador.resource.pr.TipoMovimientoPredio;

import mx.tgc.model.util.CustomSelectItem;
import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.JboException;

import org.apache.myfaces.trinidad.context.RequestContext;


public class CataAsignacionElementosBean extends GenericBean {
    /**
     * Atributos
     */
    private List elementosAsignados;
    private List accionesAsignadas;
    PropertiesReader properties;
    private List<Elemento> elementosAnteriores;
    private List<Accion> accionesAnteriores;

    /**
     * This is the default constructor (do not remove).
     */
    public CataAsignacionElementosBean() {
        if (properties == null) {
            try {
                properties =
                        new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                             CataAsignacionElementosBean.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * M�todo al que entra como parte del ciclo de vida de ADF.
     * Se valida que la llamada no sea postback para obtener todos los elementos
     * y acciones disponibles para los shuttle
     *
     * @params LifecycleContext lifecycleContext
     * @author Oscar Porres
     * @versi�n 2.0  16/12/2013
     * @component Cat�logo de asignacion de elementos
     */
    @Override
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            OperationBinding op =
                this.getBindings().getOperationBinding("getAllElementos");
            op.execute();
            if (!op.getErrors().isEmpty()) {
                throw new RuntimeException(op.getErrors().get(0).toString());
            } else {
                List<Elemento> lista = (List<Elemento>)op.getResult();
                List<SelectItem> items = this.buildSelectElementos(lista);
                this.getPageFlowScope().put("allElementos", items);
            }

            op = this.getBindings().getOperationBinding("getAllAcciones");
            op.execute();
            if (!op.getErrors().isEmpty()) {
                throw new RuntimeException(op.getErrors().get(0).toString());
            } else {
                List<Accion> lista = (List<Accion>)op.getResult();
                List<SelectItem> items = this.buildSelectAcciones(lista);
                this.getPageFlowScope().put("allAcciones", items);
            }

            String debug = super.getParametroGeneral("AA0008");
            if(debug == null && debug.equals("")){
                String msj = properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);          
            }
        }
    }
    
    
    /**
     * Construye objetos de tipo SelectItem a partir de objetos 
     * tipo Elementos
     *
     * @param List<Elemento>, lista de elementos
     *
     * @return List<SelectItem> Lista de select items
     * @author Oscar Porres
     * @versi�n 2.0  16/12/2013
     * @component Cat�logo de asignacion de elementos
     */
    public List<SelectItem> buildSelectElementos(List<Elemento> elementos){
        List<SelectItem> items = new ArrayList<SelectItem>();
        SelectItem item = null;
        String value = null;
        for(Elemento ele : elementos){
            item = new SelectItem();
            value = ele.getSerie() + "-" + ele.getIdentificador();
            item.setDescription(ele.getDescripcion());
            item.setLabel(ele.getNombre());
            item.setValue(value);
            items.add(item);
        }
        return items;
    }
    
    /**
     * Construye objetos de tipo SelectItem a partir de objetos 
     * tipo Accion
     *
     * @param List<Accion>, lista de elementos
     *
     * @return List<SelectItem> Lista de select items
     * @author Oscar Porres
     * @versi�n 2.0  16/12/2013
     * @component Cat�logo de asignacion de elementos
     */
    public List<SelectItem> buildSelectAcciones(List<Accion> acciones){
        List<SelectItem> items = new ArrayList<SelectItem>();
        SelectItem item = null;
        String value = null;
        for(Accion ele : acciones){
            item = new SelectItem();
            value = ele.getSerie() + "-" + ele.getIdentificador();
            item.setDescription(ele.getDescripcion());
            item.setLabel(ele.getNombre());
            item.setValue(value);
            items.add(item);
        }
        return items;
    }
    
    /**
     * Obtiene los elementos asignados al tipo de movimiento seleccionado
     * se almacenan los elementos en una variable global para enviarse cuando
     * se haga algun cambio y asi poder crear la auditoria correspondiente
     *
     * @return List, lista de elementos asignados
     * @author Oscar Porres
     * @versi�n 2.0  16/12/2013
     * @component Cat�logo de asignacion de elementos
     */
    public List getElementosAsignados() {
        FacesContext context = FacesContext.getCurrentInstance();
        List<String> elemAsignados = new ArrayList<String>();
        List<Elemento> itemsAsignados = null;

        String movimiento = this.getMovimientoSeleccionado();
        
        if (movimiento == null || movimiento.equals("")) {
            context.addMessage(null,
                               new FacesMessage(properties.getMessage("SYS.MOVIMIENTO")));
            return elemAsignados;
        }
        
        StringTokenizer st = new StringTokenizer(movimiento, "-");
        Integer serie = Integer.valueOf(st.nextToken());
        Long id = Long.valueOf(st.nextToken());

        OperationBinding op =
            getBindings().getOperationBinding("getElementosAsignados");
        op.getParamsMap().put("serie",serie);
        op.getParamsMap().put("id",id);
        op.execute();

        if (!op.getErrors().isEmpty()) {
            context.addMessage("Error",
                               new FacesMessage(properties.getMessage("SYS.OPERACION_FALLIDA")));
            return elemAsignados;
        }
        itemsAsignados = (List<Elemento>)op.getResult();
        String value = null;

        this.elementosAnteriores = itemsAsignados;
        for (Elemento ele : itemsAsignados) {
            value = ele.getSerie() + "-" + ele.getIdentificador();
            elemAsignados.add(value);
        }
        return elemAsignados;
    }
    
    /**
     * Obtiene las acciones asignadas al tipo de movimiento seleccionado
     * se almacenan las acciones en una variable global para enviarse cuando
     * se haga algun cambio y asi poder crear la auditoria correspondiente
     *
     * @return List, lista de elementos asignados
     * @author Oscar Porres
     * @versi�n 2.0  16/12/2013
     * @component Cat�logo de asignacion de elementos
     */
    public List getAccionesAsignadas(){
        FacesContext context = FacesContext.getCurrentInstance();
        List<String> accionesAsignadas = new ArrayList<String>();
        List<Accion> itemsAsignados = null;

        String movimiento = this.getMovimientoSeleccionado();
        
        if (movimiento == null || movimiento.equals("")) {
            context.addMessage(null,
                               new FacesMessage(properties.getMessage("SYS.MOVIMIENTO")));
            return accionesAsignadas;
        }
        
        StringTokenizer st = new StringTokenizer(movimiento, "-");
        Integer serie = Integer.valueOf(st.nextToken());
        Long id = Long.valueOf(st.nextToken());

        OperationBinding op =
            getBindings().getOperationBinding("getAccionesAsignadas");
        op.getParamsMap().put("serie",serie);
        op.getParamsMap().put("id",id);
        op.execute();

        if (!op.getErrors().isEmpty()) {
            context.addMessage("Error",
                               new FacesMessage(properties.getMessage("SYS.OPERACION_FALLIDA")));
            return accionesAsignadas;
        }
        itemsAsignados = (List<Accion>)op.getResult();
        String value = null;

        this.accionesAnteriores = itemsAsignados;
        for (Accion ele : itemsAsignados) {
            value = ele.getSerie() + "-" + ele.getIdentificador();
            accionesAsignadas.add(value);
        }
        return accionesAsignadas;
    }

    /**
     * Se encarga de guardar los cambios que se hayan realizado en las asignaciones
     * tanto de elementos como de acciones.
     * Muestra mensaje de acuerdo al resultado de la operacion
     *
     * @return String, transicion
     * @author Oscar Porres
     * @versi�n 2.0  16/12/2013
     * @component Cat�logo de asignacion de elementos
     */
    public String procesarCambios() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            String movimiento = this.getMovimientoSeleccionado();
            if (movimiento == null || movimiento.equals("")) {
                context.addMessage(null,
                                   new FacesMessage(properties.getMessage("SYS.MOVIMIENTO")));
                return null;
            }
            StringTokenizer st = new StringTokenizer(movimiento, "-");
            Integer serie = Integer.valueOf(st.nextToken());
            Long id = Long.valueOf(st.nextToken());


            List values =
                this.elementosAsignados != null ? elementosAsignados :
                new ArrayList();

            // se crea un mapa para las auditorias, ya que las llaves son
            //diferentes a como se tratan en PublicKeys
            Map datosUsuario = this.getInfoUser();
            Map datosAuditoria = new HashMap();
            datosAuditoria.put("userName",
                               datosUsuario.get(PublicKeys.LOGIN_USER));
            datosAuditoria.put("ipAddress",
                               datosUsuario.get(PublicKeys.LOGIN_IP));
            datosAuditoria.put("host",
                               datosUsuario.get(PublicKeys.LOGIN_HOST));

            OperationBinding op =
                getBindings().getOperationBinding("insertaElementosMovimientos");
            op.getParamsMap().put("eleAsignados", values);
            op.getParamsMap().put("eleAnteriores", elementosAnteriores);
            op.getParamsMap().put("datosAuditoria", datosAuditoria);
            op.getParamsMap().put("movSerie", serie);
            op.getParamsMap().put("movId", id);
            op.execute();
            
            if (!op.getErrors().isEmpty()) {
                throw new JboException(op.getErrors().get(0).toString());
            }
            
            values =
                this.accionesAsignadas != null ? accionesAsignadas :
                new ArrayList();
            
            op =
                getBindings().getOperationBinding("insertaAccionesMovimientos");
            op.getParamsMap().put("accionesAsignadas", values);
            op.getParamsMap().put("accionesAnteriores", accionesAnteriores);
            op.getParamsMap().put("datosAuditoria", datosAuditoria);
            op.getParamsMap().put("movSerie", serie);
            op.getParamsMap().put("movId", id);
            op.execute();
            
            if (!op.getErrors().isEmpty()) {
                throw new JboException(op.getErrors().get(0).toString());
            }
            
            context.addMessage(null,
                               new FacesMessage(properties.getMessage("SYS.OPERACION_EXITOSA")));

        } catch (Exception e) {
            context.addMessage(null,
                               new FacesMessage(properties.getMessage("SYS.OPERACION_FALLIDA")));
            return null;
        }
        return null;
    }


    /**
     * Este m�todo privado es llamado cada vez que se obtienen los elementos
     * seleccionados, las acciones seleccionados o al procesar cambios, ya que
     * contiene la l�gica para obtener la Serie y el Identificador del movimiento
     * que se va a mostrar en pantalla. Regresa un String que ser� usado para
     * mostrarlo en el "SelectOneChoice" de la pantalla y llev�rselo a
     * los m�todos mencionados previamente.
     *
     * @return    mov         El String que concatena la serie y el identificador del movimiento
     * @author    enunez
     * @date      06/09/2011
     */
    private String getMovimientoSeleccionado() {
        oracle.binding.AttributeBinding movSerie =
            (oracle.binding.AttributeBinding)getBindings().getControlBinding("Serie");
        oracle.binding.AttributeBinding movId =
            (oracle.binding.AttributeBinding)getBindings().getControlBinding("Identificador");
        String mov = "";
        if ((movSerie.getInputValue() != null ||
             movSerie.getInputValue().toString().equals("")) &&
            (movId.getInputValue() != null ||
             movId.getInputValue().toString().equals(""))) {
            mov = movSerie.getInputValue().toString() + "-" + movId.getInputValue().toString();
        }
        return mov;
    }

    public void setElementosAsignados(List elementosAsignados) {
        this.elementosAsignados = elementosAsignados;
    }

    public void setAccionesAsignadas(List accionesAsignadas) {
        this.accionesAsignadas = accionesAsignadas;
    }
}
