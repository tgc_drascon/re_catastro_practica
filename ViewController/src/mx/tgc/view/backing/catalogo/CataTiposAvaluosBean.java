package mx.tgc.view.backing.catalogo;

import java.io.IOException;

import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.DMLConstraintException;
import oracle.jbo.JboException;

import oracle.jbo.Row;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class CataTiposAvaluosBean extends GenericBean {
    private RichPopup borrarPopUp;
    private RichPopup cancelarPopUp;
    private PropertiesReader pr;
    private RichPopup popupDml;
    private RichSelectOneChoice municipios;
    private PropertiesReader properties;

    public CataTiposAvaluosBean() {
        if (pr == null) {
            try {
                pr =
 new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                      CataTiposAvaluosBean.class);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public void setBorrarPopUp(RichPopup borrarPopUp) {
        this.borrarPopUp = borrarPopUp;
    }

    public RichPopup getBorrarPopUp() {
        return borrarPopUp;
    }

    /**
     * Borra un registro del catalogo y luego le hace commit
     * @return
     */
    public String borrarRegistro() {
        FacesContext fc = FacesContext.getCurrentInstance();
        BindingContainer bindings = getBindings();
        try {
            OperationBinding delete = bindings.getOperationBinding("Delete");
            OperationBinding commit = bindings.getOperationBinding("Commit");
            OperationBinding rollback =
                bindings.getOperationBinding("Rollback");
            delete.execute();
            String msj = "";
            List deleteErr = delete.getErrors();

            if (!deleteErr.isEmpty()) {
                rollback.execute();
                msj = pr.getMessage("SYS.OPERACION_FALLIDA");
                fc.addMessage(null, new FacesMessage(msj));
            } else {
                commit.execute();
                List<Exception> commitErr = commit.getErrors();
                if (!commitErr.isEmpty()) {
                    String causa = commitErr.get(0).getCause().toString();
                    if (causa.contains("ORA-02292")) {
                        msj = pr.getMessage("SYS.REGISTRO_ASOCIADO");
                    } else {
                        msj = pr.getMessage("SYS.OPERACION_FALLIDA");
                    }
                    rollback.execute();

                    fc.addMessage(null, new FacesMessage(msj));
                } else {
                    msj = pr.getMessage("SYS.OPERACION_EXITOSA");
                    fc.addMessage(null, new FacesMessage(msj));
                }
            }
        } catch (DMLConstraintException dmle) {
            throw new JboException(dmle.getCause().toString());
        }
        return null;
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setCancelarPopUp(RichPopup cancelarPopUp) {
        this.cancelarPopUp = cancelarPopUp;
    }

    public RichPopup getCancelarPopUp() {
        return cancelarPopUp;
    }

    @Override
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);
        if (!RequestContext.getCurrentInstance().isPostback()) {
            //Obtiene la recaudación base del usuario para filtrar la tabla
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            if ("".equals(idsMunicipios)) {
                muestraPopup("popupRecBase");
            }
            
            BindingContainer bindings = getBindings();
            OperationBinding ob1 = bindings.getOperationBinding("filtrarMunicipiosXUsuario");
            ob1.getParamsMap().put("muniId", idsMunicipios);
            ob1.execute();

            String debug = super.getParametroGeneral("AA0008");
            if (debug == null && debug.equals("")) {
                String msj =
                    properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }
        }
    }

    /**
     * Método para hacer el commit de la transacción, primero asigna la
     * recaudación del usuario firmado al registro.
     * @return
     */
    public String insertarRegistro() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
        DCBindingContainer bc =
            (DCBindingContainer)elFactory.createValueExpression(elctx, "#{bindings}", DCBindingContainer.class).getValue(elctx);
        DCIteratorBinding it = bc.findIteratorBinding("AaUsuariosMunicipiosVVO1Iterator");
        for (int i = 0; i < it.getViewObject().getEstimatedRowCount(); i++) {
            Row r = it.getRowAtRangeIndex(i);
            String clave = (String)r.getAttribute("ClaveMunicipio");
            String seleccion = municipios.getValue().toString();
            if (seleccion.equals(clave)) {
                AttributeBinding PcMuniIdentificador =
                    (AttributeBinding)getBindings().getControlBinding("PcMuniIdentificador");
                PcMuniIdentificador.setInputValue(r.getAttribute("Identificador"));
            }
        }
        BindingContainer bindings = getBindings();
        OperationBinding ob = bindings.getOperationBinding("Commit");
        ob.execute();
        try {
            pr =
 new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                      CataTiposAvaluosBean.class);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        if (!ob.getErrors().isEmpty()) {
            String msj = msj = pr.getMessage("SYS.OPERACION_FALLIDA");
            fc.addMessage(null, new FacesMessage(msj));
            return null;
        } else {
            String msj = msj = pr.getMessage("SYS.OPERACION_EXITOSA");
            fc.addMessage(null, new FacesMessage(msj));
            ocultaPopup();
        }
        return null;
    }

    public void formaPopupFetchListener(PopupFetchEvent popupFetchEvent) {
        AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
        Map pfs = fc.getPageFlowScope();
        String operacion = pfs.get("operacion").toString();
        if (operacion != null && operacion.equals("nuevo")) {
            OperationBinding operationBinding =
                getBindings().getOperationBinding("Create");
            operationBinding.execute();
        }
    }

    public void setPopupDml(RichPopup popupDml) {
        this.popupDml = popupDml;
    }

    public RichPopup getPopupDml() {
        return popupDml;
    }

    public void muestraPopListener(ActionEvent actionEvent) {
        muestraPopup();
    }

    public String muestraPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").show();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operación", null));
        }
        return null;
    }

    public String ocultaPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operación", null));
        }
        return null;
    }
    
    public void cancelAction(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
          BindingContainer bindings = getBindings();
          OperationBinding operationBinding2 =
            bindings.getOperationBinding("Rollback");
          operationBinding2.execute();
          AttributeBinding op1 =
            (AttributeBinding)getBindings().getControlBinding("var_modifica");
          op1.setInputValue("false");
        }
    }
    
    /**
      * Fecha:08/05/2015
      * Origen:RE - Catastro - MR0017_1 - DGO - Características valuación - Al cancelar un alta y volver a entrar a esa sección. El sistema muestra los valores de alta cancelada
      * Autor:Brenda Gomez.
      * */
    public void cerrarPopupDml(ActionEvent actionEvent){
        popupDml.cancel();
        resetComponent(popupDml);
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
        
    }


    public void setMunicipios(RichSelectOneChoice municipios) {
        this.municipios = municipios;
    }

    public RichSelectOneChoice getMunicipios() {
        return municipios;
    }
}
