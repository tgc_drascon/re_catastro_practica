package mx.tgc.view.backing.catalogo;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;
import mx.tgc.utilityfwk.view.search.SearchComponent;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class CataCaractValuacionBean extends GenericBean {
    private RichTable rt;
    private PropertiesReader pr;
    private RichPopup poupDml;
    private RichPopup popupBorrar;
    private RichPopup cancelarPopUp;
    private RichDialog confirmaPopUp;
    PropertiesReader properties;

    public void setRt(RichTable rt) {
        this.rt = rt;
    }

    public RichTable getRt() {
        return rt;
    }

    public CataCaractValuacionBean() {
        if (properties == null) {
          try {
            properties =
                new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                     CataAsentamientosBean.class);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
    }


    public String insertarRegistro() {
        commitOperation();
        ocultaPopup();
        return null;
    }

    /**
     * Este m�todo carga la lista de componentes necesarios para el panel de b�squeda
     * Los componentes se obtienen de PR_PREDIAL_CARACTERISTICAS y son:
     * SCNombre : RichInputText
     * SCEtiqueta : RichInputText
     * SCNivel : RichSelectOneChoice
     * SCComportamiento : RichSelectOneChoice
     * SCBuscar : Button
     * Nota: El prefijo SC viene de Search Component.
     *
     * @return lista Lista de componentes a renderizar en el panel de b�squeda
     */
    public List<SearchComponent> prepararBusqueda() {
        this.setColumnas(4);
        List<SearchComponent> l = new ArrayList<SearchComponent>();
        SearchComponent sc = new SearchComponent("CataCaractValuacionBean");
        sc.setComponente(SearchComponent.INPUT_TEXT);
        sc.setAtributo("Nombre");
        sc.setLabel("Nombre:");
        sc.setId("txtNombre");

        SearchComponent sc1 = new SearchComponent("CataCaractValuacionBean");
        sc1.setComponente(SearchComponent.INPUT_TEXT);
        sc1.setAtributo("Etiqueta");
        sc1.setLabel("Etiqueta:");
        sc1.setId("txtEtiqueta");

        SearchComponent sc2 = new SearchComponent("CataCaractValuacionBean");
        sc2.setComponente(SearchComponent.SELECT_ONE_CHOICE);
        sc2.setDominio("PR_PREDIAL_CARACTERISTICAS.NIVEL");
        sc2.setAtributo("Nivel");
        sc2.setLabel("Nivel:");
        sc2.setId("socNivel");

        SearchComponent sc3 = new SearchComponent("CataCaractValuacionBean");
        sc3.setComponente(SearchComponent.SELECT_ONE_CHOICE);
        sc3.setDominio("PR_PREDIAL_CARACTERISTICAS.COMPORTAMIENTO");
        sc3.setAtributo("Comportamiento");
        sc3.setLabel("Comportamiento:");
        sc3.setId("socComportamiento");

        l.add(sc);
        
        String catalogo=(String)this.getPageFlowScope().get("catalogo");
        if(catalogo!=null || !catalogo.equals("false")){
            l.add(sc1);
        }
       
        l.add(sc2);
        l.add(sc3);
        return l;
    }


    public String subirValores() {
        CollectionModel _tableModel = (CollectionModel)rt.getValue();
        JUCtrlHierBinding _adfTableBinding =
            (JUCtrlHierBinding)_tableModel.getWrappedData();
        _adfTableBinding.getDCIteratorBinding();
        Object _selectedRowData = rt.getSelectedRowData();
        JUCtrlHierNodeBinding _nodeBinding =
            (JUCtrlHierNodeBinding)_selectedRowData;
        AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
        Map scopeVar = facesCtx.getPageFlowScope();
        scopeVar.put("serie", _nodeBinding.getAttribute("Serie").toString());
        scopeVar.put("nombre", _nodeBinding.getAttribute("Nombre").toString());
        scopeVar.put("identificador", _nodeBinding.getAttribute("Identificador").toString());
        this.setSearchComponents(prepararBusqueda());
        ejecutarRestablecerGenerico();
        return "exit";
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    @Override
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);
        if (!RequestContext.getCurrentInstance().isPostback()) {
            BindingContainer bindings = getBindings();
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
            Map scopeVar = facesCtx.getPageFlowScope();
            String s = (String)scopeVar.get("catalogo");
            OperationBinding filtrarActivos =
                bindings.getOperationBinding("filtrarActivos");
            filtrarActivos.getParamsMap().put("view",
                                              "PrPredialCaracteristicasVO1");

            if (s == null) {
                scopeVar.put("catalogo", "true");
                filtrarActivos.getParamsMap().put("filtrar", false);
            } else {
                scopeVar.put("catalogo", "false");
                filtrarActivos.getParamsMap().put("filtrar", true);
            }
            filtrarActivos.execute();
            
            OperationBinding filtrarNivel =
                            bindings.getOperationBinding("filtrarNivel");
            String n = (String)scopeVar.get("filtrarNivel");
            if(n == null){
                filtrarNivel.getParamsMap().put("nivel","false");
            } else {
                filtrarNivel.getParamsMap().put("nivel", n);
            }
            filtrarNivel.execute();
            
          OperationBinding filtrarPorMunicipio =
              bindings.getOperationBinding("filtrarMunicipiosActivos");
          if (idsMunicipios.equalsIgnoreCase("") || idsMunicipios== null){
              filtrarPorMunicipio.getParamsMap().put("muniId", "false");
          }  else{
              if(scopeVar.get("municipioSeleccionado") != null){
                  idsMunicipios = scopeVar.get("municipioSeleccionado").toString();
              }
              filtrarPorMunicipio.getParamsMap().put("muniId", idsMunicipios);
              filtrarPorMunicipio.execute();
          }
        }
    }


    public void cambiaAvaluo(ValueChangeEvent valueChangeEvent) {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        operationBinding.execute();

        AttributeBinding op1 =
            (AttributeBinding)getBindings().getControlBinding("puedeModificar");
        op1.setInputValue("false");
    }

    /**
     *  M�todo para realizar el commit de operaciones, el cual indica mediante un
     *  mensaje que la operaci�n fu� exitosa o que ocurri� un error.
     *  @autor Ang�lica Ledezma
     */
    private void commitOperation() {
        OperationBinding operationBinding =
            getBindings().getOperationBinding("Commit");
        operationBinding.execute();
        String msj = "";
        try {
            pr =
 new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                      CataTiposAvaluosBean.class);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        if (!operationBinding.getErrors().isEmpty()) {
            FacesContext fc = FacesContext.getCurrentInstance();
            msj = pr.getMessage("SYS.OPERACION_FALLIDA");
            fc.addMessage(null, new FacesMessage(msj));
            rollbackOperation();
        } else {
            msj = pr.getMessage("SYS.OPERACION_EXITOSA");
            mostrarMensaje(FacesMessage.SEVERITY_INFO, msj);
        }
    }

    private void rollbackOperation() {
        OperationBinding operationBinding =
            getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
    }

      public String borrarRegistro() {
        BindingContainer bindings=getBindings();
        OperationBinding ob = bindings.getOperationBinding("Delete");
        ob.execute();

        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Commit");
        operationBinding2.execute();
        System.out.println(ob.getErrors());
        ob.getErrors();
        
        if (!ob.getErrors().isEmpty() || !operationBinding2.getErrors().isEmpty()) {
          OperationBinding operationBinding3 =
            bindings.getOperationBinding("Rollback");
          
          operationBinding3.execute();
          FacesContext fc = FacesContext.getCurrentInstance();
            if (operationBinding2.getErrors().toString().contains("PR_PRPC_PR_PRCA_FK")) {
                mostrarMensaje(FacesMessage.SEVERITY_WARN,
                               properties.getMessage("SYS.REGISTRO_CON_BA"));
            } else {
                mostrarMensaje(FacesMessage.SEVERITY_ERROR,
                               properties.getMessage("SYS.OPERACION_FALLIDA"));
            }
        } else {
          mostrarMensaje(FacesMessage.SEVERITY_INFO,
                         properties.getMessage("SYS.OPERACION_EXITOSA"));
          return null;
        }

        return null;      
    }

    /**
     * M�todo que ejecuta el OperationBinding Delete de la vista
     * PrValoresUbicacionesVO1 del m�dulo CataSegCatastralesAM.
     */
    private void deleteOperation() {
        try {
            OperationBinding Delete =
                getBindings().getOperationBinding("Delete");
            Delete.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String muestraPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.poupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").show();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operaci�n", null));
        }
        return null;
    }

    public void muestraPopListener(ActionEvent actionEvent) {
        muestraPopup();
    }

    public void setPoupDml(RichPopup poupDml) {
        this.poupDml = poupDml;
    }

    public RichPopup getPoupDml() {
        return poupDml;
    }

    public void formaPopupFetchListener(PopupFetchEvent popupFetchEvent) {
        AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
        Map pfs = fc.getPageFlowScope();
        String operacion = pfs.get("operacion").toString();
        if (operacion != null && operacion.equals("nuevo")) {
            OperationBinding operationBinding =
                getBindings().getOperationBinding("CreateInsert");
            operationBinding.execute();
        }
    }

    public void ocultaPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.poupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operaci�n", null));
        }
    }
    
    /**
     * Se obtiene el panel de b�squeda para el cat�logo. Se sobreescribe el
     * comportamiento de alineado de los campos de b�squeda para que queden
     * apegados al bosquejo.
     * @return RichPanelGroupLayout panel que contiene los campos de b�squeda
     * @author Isaac S�as Guti�rrez
     * @versi�n 1.0 04-01-2013
     * @component Cat�logo caracter�sticas valuaci�n
     */
    public RichPanelGroupLayout getPanelBusqueda() {
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            this.setSearchComponents(prepararBusqueda());
        }
        RichPanelGroupLayout rpgl = this.createDynamicPanelForm();
        rpgl.setHalign(rpgl.HALIGN_CENTER);
        rpgl.setLayout(rpgl.LAYOUT_VERTICAL);
        
        return rpgl;
    }

    public RichPopup getCancelarPopUp() {
        return cancelarPopUp;
    }

    public void setCancelarPopUp(RichPopup cancelarPopUp) {
        this.cancelarPopUp = cancelarPopUp;
    }
    
    /**
      * Fecha:08/05/2015
      * Origen:RE - Catastro - MR0017_1 - DGO - Caracter�sticas valuaci�n - Al cancelar un alta y volver a entrar a esa secci�n. El sistema muestra los valores de alta cancelada
      * Autor:Brenda Gomez.
      * */
    public void cancelAction(DialogEvent dialogEvent) {
      if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
        AttributeBinding op1 =
          (AttributeBinding)getBindings().getControlBinding("puedeModificar");
        op1.setInputValue("false");
      }
    }
    
    /**
      * Fecha:08/05/2015
      * Origen:RE - Catastro - MR0017_1 - DGO - Caracter�sticas valuaci�n - Al cancelar un alta y volver a entrar a esa secci�n. El sistema muestra los valores de alta cancelada
      * Autor:Brenda Gomez.
      * */
    public void cerrarPopupDml(ActionEvent actionEvent){
        poupDml.cancel();
        resetComponent(poupDml);
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
        
    }

    public RichDialog getConfirmaPopUp() {
        return confirmaPopUp;
    }

    public void setConfirmaPopUp(RichDialog confirmaPopUp) {
        this.confirmaPopUp = confirmaPopUp;
    }

    public RichPopup getPopupBorrar() {
        return popupBorrar;
    }

    public void setPopupBorrar(RichPopup popupBorrar) {
        this.popupBorrar = popupBorrar;
    }
}
