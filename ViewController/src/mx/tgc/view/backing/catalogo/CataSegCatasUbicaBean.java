package mx.tgc.view.backing.catalogo;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import mx.tgc.model.view.ubicacion.PrSegmentosCatastralesUbicaVORowImpl;
import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.domain.Number;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class CataSegCatasUbicaBean extends GenericBean {
    private RichOutputText nombreAsentamiento;
    private RichOutputText nombreVialidad;
    private RichSelectOneChoice municipios;
    private RichTable table;
    private RichPopup popupDml;
    private RichOutputText idVialidad;
    private RichOutputText idAsentamiento;
    private PropertiesReader propertiesReaderView;
    private PropertiesReader propertiesReaderModel;
    PropertiesReader properties;

    public CataSegCatasUbicaBean() {
        if (propertiesReaderView == null) {
            try {
                propertiesReaderView =
                        new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                             CataSegCatasUbicaBean.class);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        if (propertiesReaderModel == null) {
            try {
                propertiesReaderModel =
                        new PropertiesReader("/mx/tgc/model/ModelBundle.properties",
                                             CataSegCatasUbicaBean.class);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    /**
     * En este metodo se hara el filtrado por recaudacion de los segmentos
     * catastrales utilizando el metodo filtrarPorRecaudacion.
     * @autor Ang�lica Ledezma
     * @since 01-09-2011
     * @modified Manuel Armendariz, Se agrega consulta de parametro general
     * @fechaModificacion 03-01-2014
     */
    public void prepareModel(LifecycleContext lifecycleContext) {
        if (!RequestContext.getCurrentInstance().isPostback()) {
            //Obtiene la recaudaci�n base del usuario para filtrar la tabla
            BindingContainer bindings = getBindings();
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            if ("".equals(idsMunicipios)) {
                muestraPopup("popupRecBase");
            }
            
            OperationBinding ob = bindings.getOperationBinding("filtrarMunicipiosXUsuario");
            ob.getParamsMap().put("muniId", idsMunicipios);
            ob.execute();
            
            OperationBinding ob1 = bindings.getOperationBinding("filtraSegCataUbicaInicio");
            ob1.getParamsMap().put("claveMuni", "");
            ob1.execute();

            //Consulta de par�metro PR0009 que contendr� el valor que llevar� la
            //etiqueta configurable (Colonia o asentamiento).
            String etiqueta = super.getParametroGeneral("PR0009");
            if (etiqueta != null && !etiqueta.equals("")) {
                if (etiqueta.equals("ASENTAMIENTO") ||
                    etiqueta.equals("COLONIA")) {
                    super.getPageFlowScope().put("PR0009", etiqueta);
                } else {
                    super.getPageFlowScope().put("PR0009", "ASENTAMIENTO");
                    String msj =
                        propertiesReaderView.getMessage("SYS.PARAMETRO_PR0009_NO_EXISTE");
                    mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
                }
            } else {
                super.getPageFlowScope().put("PR0009", "ASENTAMIENTO");
                String msj =
                    propertiesReaderView.getMessage("SYS.PARAMETRO_PR0009_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }

            String debug = super.getParametroGeneral("AA0008");
            if (debug == null && debug.equals("")) {
                String msj =
                    properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }
        }
    }

    /**
     * Este m�todo determina el nombre de los atributos por los cuales se van
     * a poder realizar la b�squeda. El atributo a utilizar es el campo
     * 15(Completa) y se guarda en una lista de arreglos. As� mismo tambi�n
     * contiene la especificaci�n del Managed Bean que corresponde a este
     * cat�logo. Finalmente se a�aden estos elementos al panel de b�squeda y se
     * llama al m�todo "getPanelBusquedaTreeTable" de la clase padre para formar
     * din�micamente el panel.
     * @author  Ang�lica Ledezma
     * @date    13/09/2011
     */
    @Override
    public RichPanelGroupLayout getPanelBusquedaTreeTable() {
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            this.setTituloPanelBusquedaTreeTable(propertiesReaderView.getMessage("TIT.SECCION_BUSQUEDA") +
                                                 " " +
                                                 propertiesReaderView.getMessage("NOM.SEGMENTOS_CATASTRALES"));
            this.setTipPanelBusquedaTreeTable(propertiesReaderView.getMessage("TIP.BUSQUEDA_TREE_TABLE"));
            this.setBuscarPorTreeTable(propertiesReaderView.getMessage("LOV.BUSCAR_POR_TREE_TABLE"));
            this.setCriterioBusquedaTreeTable(propertiesReaderView.getMessage("LOV.CRITERIO_TREE_TABLE"));
            this.setParametroTreeTable(propertiesReaderView.getMessage("NOM.PARAM_TREE_TABLE"));
            this.setBotonBuscarTreeTable(propertiesReaderView.getMessage("BOTON.BUSCAR"));
            this.setColumnas(0);
            List<SelectItem> campos = new ArrayList<SelectItem>();
            campos.add(new SelectItem("ClaveSegmento",
                                      propertiesReaderModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.CLAVE_SEGMENTO")));
            campos.add(new SelectItem("Descripcion",
                                      propertiesReaderModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.DESCRIPCION")));
            campos.add(new SelectItem("Nivel",
                                      propertiesReaderModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.NIVEL")));
            campos.add(new SelectItem("TipoAttr",
                                      propertiesReaderModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.TIPO_SEGMENTO")));
            campos.add(new SelectItem("Campo15",
                                      propertiesReaderModel.getMessage("PR_SEGMENTOS_CATASTRALES.LABEL.CAMPO15")));
            setManagedBean("CataSegCatasUbicaBean");
            setCamposDeBusquedaTreeTable(campos);
        }
        return super.getPanelBusquedaTreeTable();
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    /**
     * M�todo del bot�n �Aceptar� que confirma la acci�n de borrar antes
     * de borrar debe de validar que el registro est� disponible para ser
     * borrado, por ello ejecuta el m�todo validaAntesDeBorrarUb
     * (CataSegCatastralesAMImpl), si regresa true el sistema despliega un
     * mensaje indicando que se tiene un valor ubicaci�n ligado.
     * @param dialogEvent
     * @return
     * @autor Ang�lica Ledezma
     * @since 09-09-2011
     */
    public void borrarDialogListener(DialogEvent dialogEvent) {
        String msj = "";
        BindingContainer bindings = getBindings();
        try {
            if (dialogEvent.getOutcome().name().equals("ok")) {
                Iterator iter = getTable().getSelectedRowKeys().iterator();
                if (iter != null && iter.hasNext()) {
                    JUCtrlHierNodeBinding rowData =
                        (JUCtrlHierNodeBinding)getTable().getSelectedRowData();
                    PrSegmentosCatastralesUbicaVORowImpl row =
                        (PrSegmentosCatastralesUbicaVORowImpl)rowData.getCurrentRow();
                    OperationBinding ob =
                        bindings.getOperationBinding("validaAntesDeBorrarUb");
                    ob.getParamsMap().put("segmentoSerie", row.getSerie());
                    ob.getParamsMap().put("segmentoID",
                                          row.getIdentificador());
                    Boolean valorUbicacion = (Boolean)ob.execute();
                    if (valorUbicacion == true) {
                        msj =
propertiesReaderView.getMessage("SYS.REGISTRO_UTILIZADO");
                        mostrarMensaje(FacesMessage.SEVERITY_WARN, msj);
                    } else {
                        row.remove();
                        refreshTable();
                        commitOperation();
                    }
                }
            } else if (dialogEvent.getOutcome().name().equals("cancel")) {
                rollbackOperation();
            }
        } catch (JboException e) {
            System.out.println(e.getMessage());
            FacesContext fc = FacesContext.getCurrentInstance();
            msj = propertiesReaderView.getMessage("SYS.OPERACION_FALLIDA");
            fc.addMessage(null,
                          new FacesMessage(FacesMessage.SEVERITY_ERROR, msj,
                                           e.getBaseMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            OperationBinding operationBinding3 =
                bindings.getOperationBinding("Rollback");
            operationBinding3.execute();
            FacesContext fc = FacesContext.getCurrentInstance();
            msj = propertiesReaderView.getMessage("SYS.OPERACION_FALLIDA");
            fc.addMessage(null, new FacesMessage(msj));
        }
    }

    /**
     * Metodo configurado por el bot�n que lanzar� el control flow case del
     * pop up, para ser el que reciba los valores devueltos por el pop up del
     * cat�logo de vialidades.
     * @param returnEvent
     * @autor Ang�lica Ledezma
     * @since 14/09/2011
     */
    public void vialidadesDialogListener(ReturnEvent returnEvent) {
        try {
            if (!returnEvent.getReturnParameters().isEmpty()) {
                long idVialidad = Long.parseLong(returnEvent.getReturnParameters().get("idVialidad").toString());
                long idLocalidad = Long.parseLong(returnEvent.getReturnParameters().get("idLocaVialidad").toString());
                AttributeBinding idAttVialidad =
                    (AttributeBinding)getBindings().getControlBinding("PcVialIdentificador");
                AttributeBinding idLocaAttVialidad =
                    (AttributeBinding)getBindings().getControlBinding("PcLocaIdentificador");
                idAttVialidad.setInputValue(idVialidad);
                idLocaAttVialidad.setInputValue(idLocalidad);
            } else {
                rollbackOperation();
            }
        } catch (Exception e) {
            e.printStackTrace();
            rollbackOperation();
        } finally {
            AdfFacesContext.getCurrentInstance().getPageFlowScope().remove("municipioSel");
        }
    }

    /**
     * Metodo configurado por el bot�n que lanzar� el control flow case del
     * pop up, para ser el que reciba los valores devueltos por el pop up del
     * cat�logo de asentamientos.
     * @param returnEvent
     * @autor Ang�lica Ledezma
     * @since 14/09/2011
     */
    public void asentamientosDialogListener(ReturnEvent returnEvent) {
        try {
            if (!returnEvent.getReturnParameters().isEmpty()) {
                long idAsentamiento =
                    Long.parseLong(returnEvent.getReturnParameters().get("idAsentamiento").toString());
                long idLocalidad =
                    Long.parseLong(returnEvent.getReturnParameters().get("idLocaAsentamiento").toString());
                AttributeBinding idAttAsentamiento =
                    (AttributeBinding)getBindings().getControlBinding("PcAsenIdentificador");
                AttributeBinding idLocaAttAsentamiento =
                    (AttributeBinding)getBindings().getControlBinding("PcLocaIdentificador");
                idAttAsentamiento.setInputValue(idAsentamiento);
                idLocaAttAsentamiento.setInputValue(idLocalidad);
            } else {
                rollbackOperation();
            }

        } catch (Exception e) {
            e.printStackTrace();
            rollbackOperation();
        } finally {
            AdfFacesContext.getCurrentInstance().getPageFlowScope().remove("municipioSel");
        }
    }

    /**
     * M�todo que lanza el control flow case del pop up de vialidades,
     * aqui tambi�n se debe indicar como par�metro de entrada el comportamiento
     * de cat�logo como false.
     * @return
     * @autor Ang�lica Ledezma
     * @since 14/09/2011
     */
    public String lanzaPopUpVialidades() {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("catalogo",
                                                                    "false");
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("municipioSel",
                                                                    getMunicipioSel());
        return "vialidad";
    }

    /**
     * M�todo que lanza el control flow case del pop up de asentamientos,
     * aqui tambien se debe indicar como par�metro de entrada el comportamiento
     * de cat�logo como false.
     * @return
     * @autor Ang�lica Ledezma
     * @since 14/09/2011
     */
    public String lanzaPopUpAsentamientos() {
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("catalogo",
                                                                    "false");
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("municipioSel",
                                                                    getMunicipioSel());
        return "asentamiento";
    }

    /**
     * M�todo que es lanzado de la imagen para borrar el nombre de vialidad.
     * @return
     * @autor Ang�lica Ledezma
     * @since 14/09/2011
     */
    public void limpiaNombreVialidad() {
        AttributeBinding idVialidad =
            (AttributeBinding)this.getBindings().getControlBinding("PcVialIdentificador");
        // Se valida que el nombre contenga un valor
        if (idVialidad.getInputValue() != null) {
            idVialidad.setInputValue("");
        } else {
            rollbackOperation();
        }
    }

    /**
     * M�todo que es lanzado de la imagen para borrar el nombre del
     * asentamiento.
     * @return
     * @autor Ang�lica Ledezma
     * @since 14/09/2011
     */
    public void limpiaNombreAsentamiento() {
        AttributeBinding idAsentamiento =
            (AttributeBinding)this.getBindings().getControlBinding("PcAsenIdentificador");
        // Se valida que el nombre contenga un valor
        if (idAsentamiento.getInputValue() != null) {
            idAsentamiento.setInputValue(null);
        } else {
            rollbackOperation();
        }
    }

    /**
     * M�todo que realiza la funcionalidad de insertar o editar segun sea el
     * caso a realizar en el popupDml.
     * @param popupFetchEvent
     * @autor Ang�lica Ledezma
     * @since 13/09/2011
     */
    public void editPopupFetchListener(PopupFetchEvent popupFetchEvent) {
        String clientId = popupFetchEvent.getLaunchSourceClientId();
        FacesContext fc = FacesContext.getCurrentInstance();
        if (clientId.contains("Insert")) {
            OperationBinding operationBinding =
                getBindings().getOperationBinding("CreateInsert");
            //RowTreeTable rTT = this.getRowTreeTable();
            Row row = obtenerFilaSeleccionadaTT(this.getTreeTable());
            //if (rTT == null) {
            if (row == null) {
                this.getPopupDml().cancel();
                fc.addMessage(null,
                              new FacesMessage(propertiesReaderView.getMessage("SYS.FALTA_SELECCIONAR") +
                                               " " +
                                               propertiesReaderView.getMessage("NOM.SEGMENTO_CATASTRAL")));
                return;
            } else {
                //Row row = rTT.getRow();
                operationBinding.execute();
                AttributeBinding PrSecaSerie =
                    (AttributeBinding)getBindings().getControlBinding("PrSecaSerie");
                PrSecaSerie.setInputValue(row.getAttribute("Serie"));
                AttributeBinding PrSecaIdentificador =
                    (AttributeBinding)getBindings().getControlBinding("PrSecaIdentificador");
                PrSecaIdentificador.setInputValue(row.getAttribute("Identificador"));
            }
        } else if (popupFetchEvent.getLaunchSourceClientId().contains("Edit")) {
            Iterator iter = getTable().getSelectedRowKeys().iterator();
            if (iter != null && iter.hasNext()) {
                Object row = this.getTable().getSelectedRowData();
                if (row == null) {
                    this.getPopupDml().cancel();
                    String msj =
                        propertiesReaderView.getMessage("TIP.MSG_TABLA");
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null,
                                       new FacesMessage(FacesMessage.SEVERITY_WARN,
                                                        msj, null));
                    return;
                }
            }
        }
    }
    
        //---------------------------------------------------------------------------------------------------
        public Row obtenerFilaSeleccionadaTT(RichTreeTable nombreTT) {
            Row row = null;
            JUCtrlHierNodeBinding node = obtenerNodoSeleccionadoTT(nombreTT);
            if (node != null) {
                row = node.getRow();
            }
            return row;
        }
        //----------------------------------------------------------------------------------------------------
        public JUCtrlHierNodeBinding obtenerNodoSeleccionadoTT(RichTreeTable nombreTT) {
            RowKeySet rks = nombreTT.getSelectedRowKeys();
            JUCtrlHierNodeBinding nodo = null;
            if (rks != null) {
                CollectionModel treeModel = (CollectionModel)nombreTT.getValue();
                JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)treeModel.getWrappedData();
                List firstSet = (List)rks.iterator().next();
                nodo = treeBinding.findNodeByKeyPath(firstSet);
            }
            return nodo;
        }
        //-----------------------------------------------------------------------------------------------------

    public void editPopupCancelListener(PopupCanceledEvent popupCanceledEvent) {
        rollbackOperation();
        refreshTable();
    }

    /**
     * M�todo lanzado desde el bot�n "Aceptar" del popup de insertar/editar.
     * @param dialogEvent
     */
    public void editDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            commitOperation();
            refreshTable();
        } else if (dialogEvent.getOutcome().name().equals("cancel")) {
            rollbackOperation();
            refreshTable();
        }
    }

    /**
     *  M�todo para realizar el commit de operaciones, el cual indica mediante
     *  un mensaje que la operaci�n fu� exitosa o que ocurri� un error.
     *  @autor Ang�lica Ledezma
     */
    private void commitOperation() {
        OperationBinding operationBinding =
            getBindings().getOperationBinding("Commit");
        operationBinding.execute();
        String msj = "";
        if (!operationBinding.getErrors().isEmpty()) {
            FacesContext fc = FacesContext.getCurrentInstance();
            msj = propertiesReaderView.getMessage("SYS.OPERACION_FALLIDA");
            fc.addMessage(null, new FacesMessage(msj));
            rollbackOperation();
        } else {
            msj = propertiesReaderView.getMessage("SYS.OPERACION_EXITOSA");
            mostrarMensaje(FacesMessage.SEVERITY_INFO, msj);
            AdfFacesContext.getCurrentInstance().addPartialTarget(table);
        }
    }

    /**
     * Invokes an expression
     * @param expr
     * @param returnType
     * @param argTypes
     * @param args
     * @return
     */
    public static Object invokeMethodExpression(String expr, Class returnType,
                                                Class[] argTypes,
                                                Object[] args) {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory =
            fc.getApplication().getExpressionFactory();
        MethodExpression methodExpr =
            elFactory.createMethodExpression(elctx, expr, returnType,
                                             argTypes);
        return methodExpr.invoke(elctx, args);
    }

    /**
     * Invoke an expression
     * @param expr
     * @param returnType
     * @param argType
     * @param argument
     * @return
     */
    public static Object invokeMethodExpression(String expr, Class returnType,
                                                Class argType,
                                                Object argument) {
        return invokeMethodExpression(expr, returnType,
                                      new Class[] { argType },
                                      new Object[] { argument });

    }

    public void onRowSelection(SelectionEvent selectionEvent) {
        Row rw = obtenerFilaSeleccionadaTT(getTreeTable());
        oracle.jbo.domain.Number serie = new Number(Integer.parseInt(rw.getAttribute("Serie").toString()));
        oracle.jbo.domain.Number id = new Number(Long.parseLong(rw.getAttribute("Identificador").toString()));
        filtrarUbicacion(serie, id);
    }

    private void rollbackOperation() {
        OperationBinding operationBinding =
            getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
    }

    public void setNombreAsentamiento(RichOutputText nombreAsentamiento) {
        this.nombreAsentamiento = nombreAsentamiento;
    }

    public RichOutputText getNombreAsentamiento() {
        return nombreAsentamiento;
    }

    public void setNombreVialidad(RichOutputText nombreVialidad) {
        this.nombreVialidad = nombreVialidad;
    }

    public RichOutputText getNombreVialidad() {
        return nombreVialidad;
    }

    public void setTable(RichTable table) {
        this.table = table;
    }

    public RichTable getTable() {
        return table;
    }

    public void setPopupDml(RichPopup popupDml) {
        this.popupDml = popupDml;
    }

    public RichPopup getPopupDml() {
        return popupDml;
    }

    public void setIdVialidad(RichOutputText idVialidad) {
        this.idVialidad = idVialidad;
    }

    public RichOutputText getIdVialidad() {
        return idVialidad;
    }

    public void setIdAsentamiento(RichOutputText idAsentamiento) {
        this.idAsentamiento = idAsentamiento;
    }

    public RichOutputText getIdAsentamiento() {
        return idAsentamiento;
    }

    public void cancelarListener(ActionEvent actionEvent) {
        rollbackOperation();
        ocultaPopup();
    }

    public String ocultaPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operaci�n.", null));
        }
        return null;
    }

    public void aceptarListener(ActionEvent actionEvent) {
        if (idVialidad.getValue() == null &&
            idAsentamiento.getValue() == null) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Debe seleccionar una vialidad o un asentamiento.",
                                                null));
            rollbackOperation();
        } else {
            commitOperation();
        }
    }

    public String getMunicipioSel() {
        BindingContainer bindings = BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding it = (DCIteratorBinding)bindings.get("AaUsuariosMunicipiosVVO1Iterator");
        Row r = it.getCurrentRow();
        return r.getAttribute("Identificador").toString();
    }

    public void setMunicipios(RichSelectOneChoice municipios) {
        this.municipios = municipios;
    }

    public RichSelectOneChoice getMunicipios() {
        return municipios;
    }
    
    public void filtrarUbicacion(oracle.jbo.domain.Number serie, oracle.jbo.domain.Number id) {
        try {
            BindingContainer bindings = this.getBindings();
            OperationBinding filtrarSegCataUbica = bindings.getOperationBinding("filtraSegCataUbica");
            filtrarSegCataUbica.getParamsMap().put("cataSerie", serie);
            filtrarSegCataUbica.getParamsMap().put("cataId", id);
            filtrarSegCataUbica.execute();
        } catch (Exception e) {
            throw new JboException(e.getMessage());
        }
    }

    public void cambioMunicipioUbicacion(ValueChangeEvent ve) {
        BindingContainer bindings1 = this.getBindings();
        String clave = ve.getNewValue().toString();
        OperationBinding ob1 = bindings1.getOperationBinding("filtraSegCataUbicaInicio");
        ob1.getParamsMap().put("claveMuni", clave);
        ob1.execute();
    }
}
