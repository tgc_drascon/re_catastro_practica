package mx.tgc.view.backing.catalogo;

import java.io.IOException;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import mx.tgc.model.view.movimiento.PrTiposMovimientosPrediosVORowImpl;
import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class CataTiposMovPrediosBean extends GenericBean {
  private RichPopup borrarPopup;
  private RichPopup cancelarPopUp;
  private PropertiesReader pr;
  private RichPopup popupDml;
  private RichInputText inputProcesoCobro;

    public CataTiposMovPrediosBean() {
  }

  /**
   * Este m�todo es llamado cuando se hace clic en el bot�n �Agregar�,
   * habilita los campos de captura.
   * @return
   */
  public void insertarRegistro() {
    String msj = "";
    BindingContainer bindings = getBindings();
    OperationBinding ob = bindings.getOperationBinding("Commit");
    ob.execute();
    ob.getErrors();
    try {
      pr =
          new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties", CataTiposAvaluosBean.class);
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
    if (!ob.getErrors().isEmpty()) {
      OperationBinding operationBinding =
        bindings.getOperationBinding("Rollback");
      operationBinding.execute();
      FacesContext fc = FacesContext.getCurrentInstance();
      msj = pr.getMessage("SYS.OPERACION_FALLIDA");
      fc.addMessage(null, new FacesMessage(msj));
    } else {
      msj = pr.getMessage("SYS.OPERACION_EXITOSA");
      mostrarMensaje(FacesMessage.SEVERITY_INFO, msj);
      ocultaPopup();
    }
  }

  /**
   * M�todo que habilita los campos para captura despu�s de la modificaci�n
   * se realiza el commit y se guardan los cambios.
   * @return
   */
  public String modificarRegistro() {
    return null;
  }

  public void setBorrarPopup(RichPopup borrarPopup) {
    this.borrarPopup = borrarPopup;
  }

  public RichPopup getBorrarPopup() {
    return borrarPopup;
  }

  public BindingContainer getBindings() {
    return BindingContext.getCurrent().getCurrentBindingsEntry();
  }

  /**
   * Es el m�todo del bot�n �Aceptar� que confirma la acci�n de borrar en el
   * cual se debe ejecutar el delete y luego el commit. El borrado es f�sico
   * por el cual se debe de validar que ninguna otra tabla este usando el tipo
   * de movimiento seleccionado.
   * @return
   */
  public void borrarRegistro() {
    BindingContainer bindings = getBindings();
    OperationBinding ob = bindings.getOperationBinding("Delete");
    ob.execute();

    OperationBinding operationBinding2 =
      bindings.getOperationBinding("Commit");
    operationBinding2.execute();
    try {
      pr =
          new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties", CataTiposAvaluosBean.class);
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }
    if (!ob.getErrors().isEmpty() ||
        !operationBinding2.getErrors().isEmpty()) {
      OperationBinding operationBinding3 =
        bindings.getOperationBinding("Rollback");
      operationBinding3.execute();
      FacesContext fc = FacesContext.getCurrentInstance();
      try {
        String msj = pr.getMessage("SYS.OPERACION_FALLIDA");
        fc.addMessage(null, new FacesMessage(msj));
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      String msj = pr.getMessage("SYS.OPERACION_EXITOSA");
      mostrarMensaje(FacesMessage.SEVERITY_INFO, msj);
    }
  }

  public void setCancelarPopUp(RichPopup cancelarPopUp) {
    this.cancelarPopUp = cancelarPopUp;
  }

  public RichPopup getCancelarPopUp() {
    return cancelarPopUp;
  }

  public String muestraPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                          "\").show();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }

  public void muestraPopListener(ActionEvent actionEvent) {
    muestraPopup();
  }

  public void setPopupDml(RichPopup popupDml) {
    this.popupDml = popupDml;
  }

  public RichPopup getPopupDml() {
    return popupDml;
  }

  public void formaPopupFetchListener(PopupFetchEvent popupFetchEvent) {
    AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
    String clientId = popupFetchEvent.getLaunchSourceClientId();
    Map pfs = fc.getPageFlowScope();
    String operacion = pfs.get("operacion").toString();
    if (operacion != null && operacion.equals("nuevo")) {
      OperationBinding operationBinding =
        getBindings().getOperationBinding("Create");
      operationBinding.execute();
    }
  }

  public String ocultaPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                          "\").hide();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }
  
    public void valueChangeListenerRequiereTramite(ValueChangeEvent valueChangeEvent) {
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        DCBindingContainer bindings = ((DCBindingContainer)getBindings());
        DCIteratorBinding iterBind =
            (DCIteratorBinding)bindings.get("PrTiposMovimientosPredios1Iterator");
        if (iterBind != null && iterBind.getCurrentRow() != null) {
            PrTiposMovimientosPrediosVORowImpl row =
                (PrTiposMovimientosPrediosVORowImpl)iterBind.getCurrentRow();
            if (row.getRequiereTramite() != null &&
                row.getRequiereTramite().equals("S")) {
                this.inputProcesoCobro.setRequired(true);
            } else {
                inputProcesoCobro.setRequired(false);
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.inputProcesoCobro);
        }
    }

    public void setInputProcesoCobro(RichInputText inputProcesoCobro) {
        this.inputProcesoCobro = inputProcesoCobro;
    }

    public RichInputText getInputProcesoCobro() {
        return inputProcesoCobro;
    }
    
    /**
      * Fecha:08/05/2015
      * Origen:RE - Catastro - MR0017_1 - DGO - Caracter�sticas valuaci�n - Al cancelar un alta y volver a entrar a esa secci�n. El sistema muestra los valores de alta cancelada
      * Autor:Brenda Gomez.
      * */
    public void cancelarListener(DialogEvent dialogEvent) {
        try {
            BindingContainer bindings = getBindings();
            OperationBinding rollback =
                bindings.getOperationBinding("Rollback");
            rollback.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
      * Fecha:08/05/2015
      * Origen:RE - Catastro - MR0017_1 - DGO - Caracter�sticas valuaci�n - Al cancelar un alta y volver a entrar a esa secci�n. El sistema muestra los valores de alta cancelada
      * Autor:Brenda Gomez.
      * */
    public void cerrarPopupDml(ActionEvent actionEvent){
        popupDml.cancel();
        resetComponent(popupDml);
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
        
    }
}
