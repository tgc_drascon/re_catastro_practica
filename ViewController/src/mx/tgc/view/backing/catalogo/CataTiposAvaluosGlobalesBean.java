package mx.tgc.view.backing.catalogo;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanRadio;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class CataTiposAvaluosGlobalesBean extends GenericBean {

    private RichPopup popupDml;

    private void mostrarMensajeOperacion(String keyMsg) {
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            String msj =
                new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                     CataTiposAvaluosGlobalesBean.class).getMessage(keyMsg);
            fc.addMessage(null, new FacesMessage(msj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Este m�todo sirve para insertar un registro de la tabla
     * PR_TIPOS_AVALUOS.
     * @return
     */
    public String insertarRegistro() {
        BindingContainer bindings = getBindings();
        OperationBinding ob = bindings.getOperationBinding("Commit");
        ob.execute();
        if (!ob.getErrors().isEmpty()) {
            mostrarMensajeOperacion("SYS.OPERACION_FALLIDA");
            return null;
        }
        mostrarMensajeOperacion("SYS.OPERACION_EXITOSA");
        ocultaPopup();
        return null;
    }
    
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);        
        if (!RequestContext.getCurrentInstance().isPostback()) {
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            if ("".equals(idsMunicipios)) {
                muestraPopup("popupRecBase");
            }
        }
    }

    /**
     * Este m�todo sirve para actualizar un registro de la tabla
     * PR_TIPOS_AVALUOS.
     *
     * @return
     */
    public String modificarRegistro() {
        return null;
    }

    /**
     * Este m�todo sirve para borrar un registro de la tabla
     * PR_TIPOS_AVALUOS(borrado f�sico). En caso de que el registro se est� utilizando
     * y no se pueda borrar, se deber� mostrar el mensaje de la llave
     * SYS_REGISTRO_UTILIZADO.
     * @return
     * @modified modificado por Eduardo Vidal el 22-SEP-2011
     */
    public String borrarRegistro() {
        BindingContainer bindings = getBindings();
        OperationBinding obDelete = bindings.getOperationBinding("Delete");
        obDelete.execute();

        OperationBinding obCommit = bindings.getOperationBinding("Commit");
        obCommit.execute();

        FacesContext.getCurrentInstance();
        obCommit.getErrors();

        if (!obDelete.getErrors().isEmpty() ||
            !obCommit.getErrors().isEmpty()) {
            mostrarMensajeOperacion("SYS.TIPO_AVALUO_UTILIZADO");
            OperationBinding obRollback =
                bindings.getOperationBinding("Rollback");
            obRollback.execute();
            return null;
        }
        mostrarMensajeOperacion("SYS.OPERACION_EXITOSA");
        return null;
    }

    /**
   * Es el m�todo que se ejecuta cuando se selecciona un �radio button� con
   * las diferentes opciones de filtrado para las recaudaciones, para las
   * opciones �Todas� y �Ninguna� autom�ticamente se filtra la tabla de
   * PrTiposAvaluos mandando llamar el m�todo de persistencia
   * �filtrarXRecaudacion� del m�dulo CataAvaluoAMImpl.
   * @param event
   * @versi�n 1.1
   * @component Cat�logo de tipos de aval�os globales.
   * @modify 26-12-2013 Se elimina el m�todo "filtrarXRecaudacion", por lo
   * que se manda llamar al m�todo nuevo "isRecaudacionEmpty", indicando en
   * su par�metro si se necesita filtrar los tipos de aval�os para obtenerlos
   * todos o solo los que no tienen una recaudaci�n asignada.
   * @author Isaac S�as Guti�rrez
   */

  public void onValueChange(ValueChangeEvent event) {
    RichSelectBooleanRadio sob = (RichSelectBooleanRadio)event.getSource();
    BindingContainer bindings = getBindings();
    
    if (event.getNewValue().toString().equals("true")) {
      //Se limpia la vista de tiposAvaluos para que no se quede algun criterio.
      OperationBinding limpiar = bindings.getOperationBinding("limpiarTiposAvaluos");
      limpiar.execute();
      
      int valor = 1;
      boolean renderisarTabla = false;
      if (sob.getId().equals("sbr2")) {
        valor = 2;
      } else if (sob.getId().equals("sbr3")) {
        renderisarTabla = true;
      }
      this.getProcessBean().put("renderisarTabla", renderisarTabla);
      OperationBinding oper =
        bindings.getOperationBinding("isMunicipioEmpty");
      
      try {
        switch (valor) {
        case 1:
          oper.getParamsMap().put("municipioEmpty", false);
          oper.execute();
          break;
        case 2:
          oper.getParamsMap().put("municipioEmpty", true);
          oper.execute();
          break;
        }
        if (!oper.getErrors().isEmpty()) {
          this.mostrarMensajeOperacion("SYS.OPERACION_FALLIDA");
        }
      } catch (Exception e) {
        this.mostrarMensaje(FacesMessage.SEVERITY_ERROR, e.getMessage());
      }
    }
  }

    /**
     * Es el m�todo que se ejecuta cuando se selecciona un registro la tabla de
     * recaudaciones, la cual se habilita cuando se selecciona la tercera opci�
     * n de los radio button �Recaudaci�n:�, este m�todo filtra la tabla
     * PrTiposAvaluos dependiendo de la recaudaci�n seleccionada llamando el
     * m�todo �filtrarPorRecaudacion� del m�dulo CataAvaluoAMImpl.
     */
    public String onRowSelection() {
        //obtener el id del municipio
        AttributeBinding id = (AttributeBinding)this.getBindings().getControlBinding("muniId");
        BindingContainer bindings = getBindings();
        OperationBinding ob = bindings.getOperationBinding("filtrarPorMunicipio");
        ob.getParamsMap().put("view", "PrTiposAvaluosVO1");
        ob.getParamsMap().put("muniId", Integer.parseInt(id.getInputValue().toString()));
        ob.getParamsMap().put("nullCase", false);
        ob.execute();
        return null;
    }

    /** *M�todo que obtiene y regresa el objeto pageFlowScope (ProcessScope).
     *
     * @return Map
     */
    private Map getProcessBean() {
        AdfFacesContext processBean;
        processBean = AdfFacesContext.getCurrentInstance();
        Map beanSessionMap = processBean.getPageFlowScope();
        return beanSessionMap;
    }

    /**
     * @param popupFetchEvent
     */
    public void formaPopupFetchListener(PopupFetchEvent popupFetchEvent) {
        AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
        Map pfs = fc.getPageFlowScope();
        String operacion = pfs.get("operacion").toString();
        if (operacion != null && operacion.equals("nuevo")) {
            OperationBinding operationBinding =
                getBindings().getOperationBinding("Create");
            operationBinding.execute();
        }

    }

    public void setPopupDml(RichPopup popupDml) {
        this.popupDml = popupDml;
    }

    public RichPopup getPopupDml() {
        return popupDml;
    }

    public String muestraPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").show();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operaci�n", null));
        }
        return null;
    }

    public void muestraPopListener(ActionEvent actionEvent) {
        muestraPopup();
    }

    public String ocultaPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operaci�n", null));
        }
        return null;
    }
    
    /**
      * Fecha:08/05/2015
      * Origen:RE - Catastro - MR0017_1 - DGO - Caracter�sticas valuaci�n - Al cancelar un alta y volver a entrar a esa secci�n. El sistema muestra los valores de alta cancelada
      * Autor:Brenda Gomez.
      * */
    public void cerrarPopupDml(ActionEvent actionEvent){
        popupDml.cancel();
        resetComponent(popupDml);
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
           
    }
}
