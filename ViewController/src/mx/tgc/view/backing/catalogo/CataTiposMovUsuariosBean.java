package mx.tgc.view.backing.catalogo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.net.tgc.recaudador.resource.pr.TipoMovimientoPredio;

import mx.tgc.model.util.CustomSelectItem;
import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.JboExceptionHandler;

import org.apache.myfaces.trinidad.context.RequestContext;


public class CataTiposMovUsuariosBean extends GenericBean {

    private RichPopup confirmaPopUp;
    PropertiesReader properties;
    private List<SelectItem> elementosAsignados;
    private List<TipoMovimientoPredio> elementosAnteriores;
    

    /**
     * This is the default constructor (do not remove).
     */
    public CataTiposMovUsuariosBean() {
        if (properties == null) {
            try {
                properties =
                        new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                             CataTiposMovUsuariosBean.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
    * M�todo al que entra como parte del ciclo de vida de ADF.
    * Se valida que la llamada no sea postback para obtener todos los 
    * tipos movimientos predio disponibles y subirlos para que el
    * shuttle accese a ellos
    * 
    * @param LifecycleContext context
    * 
    * @author Oscar Porres
    * @versi�n 2.0  13/12/2013
    * @component Cat�logo de tipos movimientos predios
    */
    public void prepareModel(LifecycleContext context) {
        super.prepareModel(context);
        RequestContext rc = RequestContext.getCurrentInstance();
        if(!rc.isPostback()){
            OperationBinding op =
                getBindings().getOperationBinding("getAllTiposMovimientosPredios");
            op.execute();
            if(!op.getErrors().isEmpty()){
                throw new RuntimeException(op.getErrors().get(0).toString());
            }else{
                List<TipoMovimientoPredio> lista = (List<TipoMovimientoPredio>)op.getResult();
                List<SelectItem> items = this.buildSelectItems(lista);
                this.getPageFlowScope().put("allMovimentosPredios", items);
            }

            String debug = super.getParametroGeneral("AA0008");
            if(debug == null && debug.equals("")){
                String msj = properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);          
            }
        }
    }
    
    
    /**
    * M�todo que se encarga de construir SelectItem a partir de una lista
    * de TipoMovimientoPredio para que se use por el componente Shuttle
    * 
    * @param List<TipoMovimientoPredio> lista, lista de tipos mov predios
    * @return List<SelectItem>, lista de SelectItem
    * @author Oscar Porres
    * @versi�n 2.0  13/12/2013
    * @component Cat�logo de tipos movimientos predios
    */
    private List<SelectItem> buildSelectItems(List<TipoMovimientoPredio> lista){
        List<SelectItem> items = new ArrayList<SelectItem>();
        SelectItem item = null;
        String value = null;
        for(TipoMovimientoPredio mov : lista){
            item = new SelectItem();
            value = mov.getSerie() + "-" + mov.getIdentificador();
            item.setDescription(mov.getDescripcion());
            item.setLabel(mov.getNombre());
            item.setValue(value);
            items.add(item);
        }
        return items;
    }

    /**
     * Es el m�todo que confirma la acci�n de guardar los cambios,  este m�todo
     * manda llamar al m�todo de persistencia insertaTiposMovPrediosUsuario()
     * (CatalogoSencilloAMImpl).
     * 
     * @modify Oscar Porres - 13/12/2013 - Se agregan datos de auditoria y 
     * se cambian parametros para los metodos de persistencia 
     */
    public String procesarCambios() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            String usuario = getUsuarioSeleccionado();
            if (usuario.equals("")) {
                context.addMessage(null,
                                   new FacesMessage(properties.getMessage("SYS.USUARIO")));
                return null;
            }

            OperationBinding op =
                getBindings().getOperationBinding("insertaTiposMovPrediosUsuario");

            List values =
                this.elementosAsignados != null ? elementosAsignados :
                new ArrayList();
            
            // se crea un mapa para las auditorias, ya que las llaves son
            //diferentes a como se tratan en PublicKeys
            Map datosUsuario = this.getInfoUser();
            Map datosAuditoria = new HashMap();
            
            datosAuditoria.put("userName", datosUsuario.get(PublicKeys.LOGIN_USER));
            datosAuditoria.put("ipAddress", datosUsuario.get(PublicKeys.LOGIN_IP));
            datosAuditoria.put("host", datosUsuario.get(PublicKeys.LOGIN_HOST));
            
            op.getParamsMap().put("movAsignados", values);
            op.getParamsMap().put("movAnteriores", elementosAnteriores);
            op.getParamsMap().put("datosAuditoria", datosAuditoria);
            op.execute();
            if(!op.getErrors().isEmpty()){
                throw new JboException(op.getErrors().get(0).toString());
            }
            context.addMessage(null,
                               new FacesMessage(properties.getMessage("SYS.OPERACION_EXITOSA")));

        } catch (Exception e) {
            context.addMessage(null,
                               new FacesMessage(properties.getMessage("SYS.OPERACION_FALLIDA")));
            return null;
        }
        return null;
    }


    public void setElementosAsignados(List elementosAsignados) {
        this.elementosAsignados = elementosAsignados;
    }

    
    /**
    * Obtiene los elementos asignados para utilizarlos por el componente
    * shuttle
    * 
    * @return List<String>, lista de serie-identificador de los tipos
    * movimientos seleccionados
    * 
    * @author Oscar Porres
    * @versi�n 2.0  13/12/2013
    * @component Cat�logo de tipos movimientos predios
    */
    public List<String> getElementosAsignados() {
        FacesContext context = FacesContext.getCurrentInstance();
        String usuario = getUsuarioSeleccionado();
        List<String> elemAsignados = new ArrayList<String>();
        List<TipoMovimientoPredio> itemsAsignados = null;

        if (usuario.equals("")) {
            context.addMessage(null,
                               new FacesMessage(properties.getMessage("SYS.USUARIO")));
            return elemAsignados;
        }

        BindingContainer bindings = getBindings();

        OperationBinding op =
            bindings.getOperationBinding("getTiposMovimientosAsignados");
        op.getParamsMap().put("usuario", usuario);
        op.execute();

        if (!op.getErrors().isEmpty()) {
            context.addMessage("Error",
                               new FacesMessage(properties.getMessage("SYS.OPERACION_FALLIDA")));
            return elemAsignados;
        }
        itemsAsignados = (List<TipoMovimientoPredio>)op.getResult();
        String value = null;
        
        this.elementosAnteriores = itemsAsignados;

        for (TipoMovimientoPredio mov : itemsAsignados) {
            value = mov.getSerie() + "-" + mov.getIdentificador();
            elemAsignados.add(value);
        }

        return elemAsignados;
    }
    
    private String getUsuarioSeleccionado() {
        oracle.binding.AttributeBinding usuario =
            (oracle.binding.AttributeBinding)getBindings().getControlBinding("Usuario");
        String user = "";
        if (usuario.getInputValue() != null ||
            usuario.getInputValue().toString().equals("")) {
            user = usuario.getInputValue().toString();
        }
        return user;
    }


    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setConfirmaPopUp(RichPopup confirmaPopUp) {
        this.confirmaPopUp = confirmaPopUp;
    }

    public RichPopup getConfirmaPopUp() {
        return confirmaPopUp;
    }
}
