package mx.tgc.view.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import mx.net.tgc.util.FechasUtil;

import org.apache.myfaces.trinidad.validator.DateTimeRangeValidator;


public class DateTimeRangeValidatorForString extends DateTimeRangeValidator {
    public DateTimeRangeValidatorForString() {
        super();
    }
    
  public void validate (FacesContext context, UIComponent component, 
                        java.lang.Object value) throws ValidatorException {
    Object newValue = null;
    if (value instanceof String) {
        newValue = FechasUtil.fechaPara((String) value);
    }
    else {
        newValue = value;
    }
    super.validate(context, component, newValue); 
  }
}
