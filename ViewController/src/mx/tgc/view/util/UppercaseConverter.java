package mx.tgc.view.util;

import java.util.Collection;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.apache.myfaces.trinidad.convert.ClientConverter;

public class UppercaseConverter implements Converter, ClientConverter {
    public Object getAsObject(FacesContext facesContext, UIComponent component,
                              String stringValue) {
        return stringValue.toUpperCase();
    }

    public String getAsString(FacesContext facesContext,
                              UIComponent uiComponent, Object objectValue) {
        return objectValue.toString();
    }

    public String getClientLibrarySource(FacesContext context) {
        return context.getExternalContext().getRequestContextPath() +
                                    "/catastro/javascript/FieldAsUpperCase.js";
    }

    /**
     * @return
     */
    public Collection<String> getClientImportNames() {
        return null;
    }

    public String getClientScript(FacesContext context,
                                  UIComponent component) {
        return null;
    }

    public String getClientConversion(FacesContext context,
                                      UIComponent component) {
        return ("new FieldAsUppercase()");
    }
}
